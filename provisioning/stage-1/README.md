# Stage 1 Provisioner

This is where you can build and prepare the basebox for the eventual (stage 2)
spirithunter box.  It's here that we start with a simple Gentoo installation
and then install all of the required packages and configure the services.

If you're just looking to use a development environment, you want stage 2.
