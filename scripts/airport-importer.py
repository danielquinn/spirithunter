#!/usr/bin/env python

import sys
from djangoinit import initialise

settings = initialise()

import sys,os,re
from django.contrib.gis.geos import fromstr

class AirportException(Exception):
    pass


def fixlat(lat, dir):
    if dir == "N":
        return lat
    else:
        return lat * -1


def fixlng(lng, dir):
    if dir == "E":
        return lng
    else:
        return lng * - 1


def calculate_location(input):

    location_regex = re.compile(r"([^:]+):([^:]+)::(\d+):(\d+):(\d+):(\w),(\d+):(\d+):(\d+):(\w),([\-\d]+)")
    l = re.match(location_regex, input).groups()
    country = l[0]
    city    = l[1]
    lat = fixlat(float(l[2]) + float(l[3])/60 + float(l[4])/3600, l[5])
    lng = fixlng(float(l[6]) + float(l[7])/60 + float(l[8])/3600, l[9])

    elv = int(re.sub(r"0*-", "-", l[10]))

    return country, city, fromstr("POINT(%s %s)" % (lng, lat)), elv


def fixtmp(f):

    if f == "N/A":
        return None

    return (5.0/9.0) * (float(f) - 32.0)


def check():
    from glob import glob
    for code in os.listdir("airports"):
        if glob("airports/%s/*.html" % code):
            for month in range(1,13):
                if not os.path.exists("airports/%s/%s.html" % (code,month)):
                    print code,
                    break

def main():

    from BeautifulSoup import BeautifulSoup
    from element.models import Airport, AirportWeather

    airports = sorted(os.listdir("airports"))
    temp_regex = re.compile(r" .*")

    for code in airports:

        if os.path.exists("airports/%s/1.html" % code):

            print code.lower(),

            country, city, location, elevation = calculate_location(open("airports/%s/location" % code).read())
            try:
                a = Airport.objects.get(code=code)
                a.elevation = elevation
                a.save()
            except Airport.DoesNotExist:
                pass

#            try:
#
#                year = []
#
#                for month in range(1,13):
#
#                    soup = BeautifulSoup(open("airports/%s/%s.html" % (code, month)).read())
#
#                    row = [month]
#                    for d in soup.findAll("td", attrs={"class": "lapAvgDataRow"}):
#
#                        row.append(re.sub(temp_regex, "", d.text.replace(u"\xb0F","")))
#
#                        if len(row) == 9:
#
                            # We absolutely must have the mean temperatures
#                            if row[5] == "N/A":
#                                raise AirportException(str(row) + str(year))
#
#                            year.append(row)
#                            row = [month]
#
#                airport = Airport.objects.create(
#                    country=country,
#                    city=city,
#                    location=location,
#                    elevation=elevation,
#                    code=code
#                )
#
#                for date in year:
#                    month, day, sunrise, sunset, high, low, mean, rhigh, rlow = date
#                    AirportWeather.objects.create(
#                        airport=airport,
#                        month=month,
#                        day=day,
#                        high=fixtmp(high),
#                        low=fixtmp(low),
#                        mean=fixtmp(mean),
#                        rhigh=fixtmp(rhigh),
#                        rlow=fixtmp(rlow)
#                    )
#
#                sys.stdout.flush()
#
#            except AirportException as e:
#                print "-- DEAD --",
#                pass



if __name__ == "__main__":
#    check()
    main()

