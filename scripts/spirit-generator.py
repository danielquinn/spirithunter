#!/usr/bin/env python

from random import randint
from itertools import combinations
from djangoinit import initialise

settings = initialise()

from affinity.models import Element
from spirit.models import Spirit

affinities = dict([(a.pk, a.name) for a in Element.objects.all()])

spirit_combos = []
for i in range(3):
    spirit_combos += combinations(affinities.keys(), i+1)

for combo in spirit_combos:
    name = " ".join([affinities[id] for id in combo])
    print "Creating %s" % name
    spirit = Spirit.objects.create(name=name,hp=randint(100,1000),experience=randint(100,5000))
    spirit.affinities = Element.objects.filter(pk__in=combo)
    spirit.save()
