#!/usr/bin/env python

#
# airports/
#   code/
#     location
#     jan.html
#     ...
#     dec.html
#

from djangoinit import initialise

settings = initialise()

import os, urllib2
from time import sleep

ua  = "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2b5) Gecko/20091204 Firefox/3.6b5"
src = "GlobalAirportDatabase.txt"

weather_schema = "airport"


def get_proprietary_code(city, country):

    from re import sub

    sleep(3)

    print "  Determining proprietary code for %s, %s" % (city, country)

    url = "http://www.weather.com/search/enhancedlocalsearch?where=%s,%%20%s&loctypes=1003,1001,1000,1,9,5,11,13,19,20&from=hdr_localsearch&baseurl=/weather/today/" % (
      urllib2.quote(city),
      urllib2.quote(country)
    )
    url = urllib2.urlopen(urllib2.Request(url, "", {"User-Agent" : ua})).geturl()

    if "search" in url:
        raise Exception("Weather.com has no idea what this city is")

    pcode = sub(r".*\/", "", url)

    print "    Found! %s" % pcode

    return pcode


def get_weather(dst, schema, code, city, country):

    proprietary_code = ""

    month = 1
    while month <= 12:

        sleep(3)

        if schema == "airport":
            url = "http://www.weather.com/outlook/travel/businesstraveler/wxclimatology/daily/%s:9?climoMonth=%s" % (code, month)
        elif schema == "city":
            url = "http://www.weather.com/outlook/travel/businesstraveler/wxclimatology/daily/%s?climoMonth=%s" % (proprietary_code, month)
        else:
            raise Exception("Something has gone terribly wrong")

        document = urllib2.urlopen(urllib2.Request(url, "", {"User-Agent" : ua})).read()

        if "Your Search" in document:

            # If we've already tried searching for a propritary code, it' ain't happening
            if schema == "city":
                raise Exception("No weather found.")

            # Ok, so the airport code wasn't enough, what about by city name?
            proprietary_code = get_proprietary_code(city, country)
            schema = "city"

            continue

        print "    %s" % month
        f = open(os.path.join(dst, "%s.html" % month), mode="w")
        f.write(document)
        f.close()

        month += 1


def main():

    for line in open(src).readlines():

        (code4, code3, name, city, country, lat_deg, lat_min, lat_sec, lat_dir, lng_deg, lng_min, lng_sec, lng_dir, alt) = line.split(":")

        if code3 == "N/A" or (alt == "0000" and lng_sec == 0):
            continue

        dst = os.path.join("airports", code3)
        loc = os.path.join(dst, "location")

        try:
            os.makedirs(dst)
        except OSError:
            pass

        if os.path.exists(loc):
            continue

        try:

            f = open(loc, mode="w")
            f.write("%s:%s::%s:%s:%s:%s,%s:%s:%s:%s,%s" % (country, city, lat_deg, lat_min, lat_sec, lat_dir, lng_deg, lng_min, lng_sec, lng_dir, alt))
            f.close()

            print "%s, %s (%s)" % (city, country, code3)
            get_weather(dst, weather_schema, code3, city, country)

        except Exception as e:

            print "  Exception: %s" % e

            f = open(os.path.join(dst, "errors"), mode="w")
            f.write(e.message)
            f.close()

            sleep(3)


if __name__ == "__main__":
    main()

