from django.contrib import admin

from .models import User


class UserAdmin(admin.ModelAdmin):

    list_display = ("name", "email", "last_login",)
    list_filter = ("last_login",)

admin.site.register(User, UserAdmin)
