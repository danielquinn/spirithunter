from django.core.management.base import BaseCommand, CommandError

from spirits.models import Spirit
from ...models import User


class Command(BaseCommand):

    help = "Generate some spirits and assign them to a user"

    def add_arguments(self, parser):
        parser.add_argument("user", type=str)
        parser.add_argument("spirits", type=int)
        parser.add_argument("lat", type=float)
        parser.add_argument("lng", type=float)
        parser.add_argument("--low", type=int)
        parser.add_argument("--high", type=int)

    def handle(self, *args, **options):

        spirits = Spirit.objects.create_random(
            **self._get_kwargs_from_options(options)
        )

        self._get_user(options["user"]).spirits.add(*spirits)

        self.stdout.write(
            "The following spirits were created:\n"
            "Family Level Strengths         Weaknesses         Nationalities\n"
            "------ ----- ----------------- ------------------ -------------\n"
        )
        for spirit in spirits:
            positives = spirit.elemental_strengths.filter(strength__gt=0)
            negatives = spirit.elemental_strengths.filter(strength__lt=0)
            nationalities = spirit.nationalities.all()
            self.stdout.write("{:6} {:5} {:17} {:18} {:13}".format(
                spirit.family.name,
                spirit.level,
                ",".join([s.element.name for s in positives]),
                ",".join([s.element.name for s in negatives]),
                ",".join([str(c.country.name) for c in nationalities])
            ))

    @staticmethod
    def _get_kwargs_from_options(options):

        kwargs = {
            "total": options["spirits"],
            "lat": options["lat"],
            "lng": options["lng"],
        }
        if options["low"] is not None:
            kwargs["level_low"] = options["low"]
        if options["high"] is not None:
            kwargs["level_high"] = options["high"]

        return kwargs

    @staticmethod
    def _get_user(lookup):
        try:
            return User.objects.get(pk=lookup)
        except User.DoesNotExist:
            try:
                return User.objects.get(email=lookup)
            except User.DoesNotExist:
                raise CommandError("Can't determine the user from that input")
