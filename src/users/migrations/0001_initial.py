# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-21 21:26
from __future__ import unicode_literals

import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django_extensions.db.fields
import users.managers


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('name', models.CharField(max_length=128)),
                ('url_name', models.CharField(help_text='Used for your public URL (if you choose to make your collection public)', max_length=128)),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('birth_date', models.DateField(blank=True, null=True)),
                ('url', models.URLField(blank=True)),
                ('tz', models.CharField(default='UTC', max_length=64, verbose_name='Time Zone')),
                ('is_public', models.BooleanField(default=True, help_text='Is this user visible to others?')),
                ('balance', models.PositiveIntegerField(default=0, editable=False)),
                ('tree_location', django.contrib.gis.db.models.fields.PointField(blank=True, geography=True, null=True, srid=4326)),
                ('tree_planted', models.DateTimeField(auto_now_add=True, help_text='Time at which the tree was planted')),
                ('tree_growing_time', models.PositiveIntegerField(default=24, help_text='Time, in hours, that it takes for the tree to take root')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into the admin site.', verbose_name='Staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active.', verbose_name='Active')),
                ('date_joined', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True)),
                ('date_modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True)),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'abstract': False,
            },
            managers=[
                ('objects', users.managers.UserManager()),
            ],
        ),
    ]
