from rest_framework import serializers
from .models import User


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = (
            "url", "url_name", "birth_date", "tz", "is_public", "balance",
            "tree_location", "tree_planted", "tree_growing_time",
            "date_joined", "name", "email", "is_staff"
        )
