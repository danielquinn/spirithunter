var Listing = function(pk){

  var self = this;

  var $jarred = $('<div id="spirits-jarred" class="clearfix"><h3>Jarred</h3></div>');
  var $other  = $('<div id="spirits-other" class="clearfix"><h3>Back Home</h3></div>');
  var $target = $("#items").html("");

  this.draw_spirit = function(item){

    var $elements = $('<span class="elements"></span>');
    var $facets = $('<span class="facets"></span>');
    var $nationalities = $('<span class="nationalities"></span>');

    for (var a = 0; a < item.elementals.length; a++){
      $elements.append('<img src="' + item.elementals[a].element.images["16"] + '" title="' + item.elementals[a].element.name + '" />');
    }
    for (var b = 0; b < item.facets.length; b++){
      $facets.append('<img src="' + item.facets[b].images["16"] + '" title="' + item.facets[b].name + '" />');
    }
    for (var c = 0; c < item.nationalities.length; c++){
      $nationalities.append('<span class="flag ' + item.nationalities[c].code.toLowerCase() + '" title="' + item.nationalities[c].name + '" />');
    }

    var url = window.spirithunter.urls.spirits.detail.replace("0", item.id);
    var $spirit = $(
      '<div class="item col-xs-12 col-sm-6 col-md-4 col-lg-4">' +
        '<div class="image pull-left"><a href="' + url + '"><img src="' + item.images["128"] + '" width="128" height="128" /></a></div>' +
        '<div class="name"><h4><a href="' + url + '">' + item.name + '</a></h4></div>' +
        '<div class="level">Level: <strong>' + item.level + '</strong></div>' +
        '<div class="stats">' +
          ' ATK: ' + item.attack +
          ' DEF: ' + item.defence +
          ' HP: '  + item.health_current + '/' + item.health_maximum +
          ' SP: '  + item.speed +
          ' XP: '  + item.experience +
        '</div>' +
        '<div class="icons f16">' +
          $elements.html() +
          $facets.html() +
          $nationalities.html() +
        '</div>' +
      '</div>'
    );

    if (item.activity.id == window.spirithunter.constants.activities.jarred) {
      $jarred.append($spirit);
    } else {
      $other.append($spirit);
    }

  };

  this.draw_listing = function(){

    var url = window.spirithunter.urls.api.v1.spirits.listing;
    $.getJSON(url + "?owner=" + pk, function(data){

      for (var i = 0; i < data.objects.length; i++) {
        self.draw_spirit(data.objects[i]);
      }

      $target.append($jarred);
      $target.append($other);

    });

  };
  return this;
};
