var Listing = function(pk){

  var self = this;

  var $target = $("#items").html("");

  this.draw_item = function(url, quantity){

    $.getJSON(url, function(item){
      $target.append(
        '<div class="item col-xs-12 col-sm-6 col-md-4 col-lg-4">' +
          '<div class="image pull-left"><img src="' + item.images["64"] + '" width="64" height="64" /></div>' +
          '<div class="name"><h4>' + item.name + ' (x' + quantity + ')</h4></div>' +
        '</div>'
      );
    });

  };


  this.draw_listing = function(){

    var url = window.spirithunter.urls.api.v1.inventory.listing;
    $.getJSON(url + "?user=" + pk, function(data){
      for (var i = 0; i < data.objects.length; i++) {
        var inventory_item = data.objects[i];
        self.draw_item(inventory_item.item, inventory_item.quantity);
      }
      if (!data.objects.length) {
        $target.html(
          '<div id="empty">"' +
            "<p>It appears that you don't yet have any items.</p>" +
            "<p>Items can be given to you by spirits you choose to release after defeating, and may also be purchased in the store.</p>" +
            "<p>Note that <em>some</em> items can only be purchased, and some can only be awarded after a fight.</p>" +
          '</div>'
        );
      }
    });
  };

  return this;

};
