from django import forms
from django.conf import settings
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, get_user_model
from django.core.mail import send_mail
from django.core.urlresolvers import reverse


class LoginForm(AuthenticationForm):

    next = forms.CharField(
        max_length=1024,
        widget=forms.widgets.HiddenInput,
        required=False
    )

    def __init__(self, request, *a, **kwa):

        AuthenticationForm.__init__(self, request, *a, **kwa)

        self.fields["next"].initial = request.GET.get("next", "")

        self.request = request
        self.redirect = None

    def clean_next(self):
        self.redirect = self.cleaned_data.get("next")
        return self.redirect

    def login(self):
        login(self.request, self.user_cache)


class RegistrationForm(forms.ModelForm):

    password_confirm = forms.CharField(widget=forms.widgets.PasswordInput)

    class Meta:
        model = get_user_model()
        exclude = [
            "is_referrer", "last_login", "is_superuser", "groups",
            "user_permissions", "is_staff", "is_active", "tz"
        ]

    def __init__(self, request, *a, **kwa):

        self.request = request

        forms.ModelForm.__init__(self, *a, **kwa)

        self.fields["password"].widget = forms.widgets.PasswordInput()

    def clean_email(self):
        email = self.cleaned_data.get("email")
        if get_user_model().objects.filter(email=email).exists():
            raise forms.ValidationError(
                'That email is already registered.  If you\'ve forgotten your '
                'password, you can use the "Forgot Password?" link under the '
                'login form.'
            )
        return email

    def clean(self):

        cleaned_data = self.cleaned_data

        self.instance.is_active = False

        return cleaned_data

    def save(self, *a, **kwa):

        self.instance.set_password(self.cleaned_data["password"])

        send_mail(
            "Confirm your email address on RxLenses.ca",
            "Thank you for registering with RxLenses.ca!  As we use email to "
            "send confirmations of orders, we need to confirm that the"
            "address you entered actually works, so please visit this link to "
            "activate your account:\n\n  {url}\n\nOnce you activate your "
            "account, you'll automatically be logged in, and you can start "
            "your first order.".format(
                url=self.request.build_absolute_uri(reverse(
                    "users-activation",
                    kwargs={
                        "key": "{key}.{pk}".format(
                            key=self.instance.get_activation_key(),
                            pk=self.instance.pk
                        )
                    }
                ))
            ),
            settings.DEFAULT_FROM_EMAIL,
            [self.instance.email],
            fail_silently=False,
            auth_user=settings.EMAIL_HOST_USER,
            auth_password=settings.EMAIL_HOST_PASSWORD
        )


class DetailsForm(forms.ModelForm):

    password_confirm = forms.CharField(
        widget=forms.widgets.PasswordInput,
        required=False
    )

    class Meta:
        model = get_user_model()
        exclude = [
            "is_referrer", "last_login", "is_superuser",
            "groups", "user_permissions", "is_staff", "is_active"
        ]

    def __init__(self, *a, **kwa):

        self._set_password = None

        forms.ModelForm.__init__(self, *a, **kwa)

        self.fields["password"].widget = forms.widgets.PasswordInput()
        self.fields["password"].required = False

    def clean_email(self):

        email = self.cleaned_data.get("email")

        if self.instance.email == email:
            return email  # No change

        if get_user_model().objects.filter(email=email).exists():
            raise forms.ValidationError(
                "That email is already registered to another account"
            )

        return email

    def clean(self):

        cleaned_data = self.cleaned_data

        password = cleaned_data.get("password")
        password_confirm = cleaned_data.get("password_confirm")

        if password and password_confirm:
            self._set_password = password

        for key in ("password", "password_confirm"):
            if key in cleaned_data:
                del(cleaned_data[key])

        return cleaned_data

    def save(self, *a, **kwa):

        if self._set_password:
            self.instance.set_password(self._set_password)

        forms.ModelForm.save(self, *a, **kwa)


class ForgotForm(forms.Form):

    email = forms.EmailField()

    def __init__(self, request, *a, **kwa):

        forms.Form.__init__(self, *a, **kwa)

        self.request = request
        self.user = None

    def clean_email(self):

        email = self.cleaned_data.get("email")

        user_model = get_user_model()
        try:
            self.user = user_model.objects.get(email=email)
        except user_model.DoesNotExist:
            raise forms.ValidationError(
                "Apologies, but it appears that this email address is not in "
                "our database."
            )

        return email

    def send(self):
        new_password = self.user.reset_password()
        send_mail(
            subject="Your new password is: {password}\n\n",
            message="When you're ready, you can visit {url} and login with "
                    "your new password, at which point you can change your "
                    "password by visiting the account update page (just click "
                    "your name in the upper-right corner of any page).\n"
                    "\n".format(
                        password=new_password,
                        url=self.request.build_absolute_uri(reverse("login"))
                    ),
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[self.user.email],
            fail_silently=False,
            auth_user=settings.EMAIL_HOST_USER,
            auth_password=settings.EMAIL_HOST_PASSWORD,
        )
