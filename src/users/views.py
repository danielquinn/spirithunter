import logging

from django.http import HttpResponseRedirect
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model, get_backends, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.template.defaultfilters import mark_safe
from django.views.generic import (
    DetailView, TemplateView, FormView, View, RedirectView
)

from rest_framework import viewsets

from .forms import LoginForm, RegistrationForm, DetailsForm, ForgotForm
from .models import User
from .serializers import UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class IndexView(LoginRequiredMixin, FormView):
    """
    User details/modification page
    """

    template_name = "users/index.html"

    form_class = DetailsForm

    def get_form(self, form_class=None):
        return self.get_form_class()(
            instance=self.request.user, **self.get_form_kwargs())

    def form_valid(self, form):
        form.save()
        messages.info(self.request, "Your information has been updated")
        return FormView.form_valid(self, form)

    def get_success_url(self):
        return reverse("users-index")


class StuffListingMixin(object):

    slug_url_kwarg = "url_name"
    slug_field = "url_name"
    model = settings.AUTH_USER_MODEL

    def get_context_data(self, **kwargs):

        if not self.object.is_public:
            if not self.request.user == self.object:
                raise PermissionDenied("This user either doesn't exist or "
                                       "their account is private")

        context = DetailView.get_context_data(self, **kwargs)

        permissions = {
            "is_administrator": self.request.user == self.object
        }
        context.update({
            "permissions": permissions
        })
        return context


class SpiritListingView(LoginRequiredMixin, StuffListingMixin, DetailView):
    template_name = "users/spirits.html"


class InventoryListingView(LoginRequiredMixin, StuffListingMixin, DetailView):
    template_name = "users/inventory.html"


class ForgotView(FormView):

    template_name = "users/forgot.html"
    form_class = ForgotForm

    def get_success_url(self):
        return reverse("login")

    def get_form(self, form_class=None):
        return self.get_form_class()(
            instance=self.request, **self.get_form_kwargs())

    def form_valid(self, form):
        form.send()
        messages.info(
            self.request,
            "Thank you.  You should receive an email with a new temporary "
            "password shortly."
        )
        return HttpResponseRedirect(reverse("login"))


class RegistrationView(TemplateView):

    template_name = "users/register.html"

    def get_context_data(self, **kwargs):

        context = TemplateView.get_context_data(self, **kwargs)
        context.update({
            "login_form": LoginForm(self.request),
            "registration_form": RegistrationForm(self.request),
            "header": "Join Us",
            "subheader": "Start a collection!",
        })

        return context

    def get(self, request, *args, **kwargs):

        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse("users-index"))

        return TemplateView.get(self, request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse("users-index"))

        context = self.get_context_data(**kwargs)

        if self.request.POST.get("login"):
            context["login_form"] = LoginForm(self.request, self.request.POST)
            if context["login_form"].is_valid():
                context["login_form"].login()
                redirect = context["login_form"].redirect
                if not redirect:
                    redirect = reverse("default")
                return HttpResponseRedirect(redirect)

        if self.request.POST.get("register"):
            context["registration_form"] = RegistrationForm(
                self.request,
                self.request.POST
            )
            if context["registration_form"].is_valid():
                context["registration_form"].save()
                return HttpResponseRedirect(reverse("users-confirm"))

        return self.render_to_response(context)


class ActivationView(View):

    def get(self, request, *args, **kwargs):

        user_model = get_user_model()
        backend = get_backends()[0]

        try:
            key, pk = kwargs.get("key").split(".")
            user = user_model.objects.get(pk=pk)
            assert user.is_active == False
            assert user.get_activation_key() == key
        except (ValueError, user_model.DoesNotExist, AssertionError):
            return self.fail_out()

        user_model.objects.filter().update(is_active=True)
        user.backend = "%s.%s" % (
            backend.__module__,
            backend.__class__.__name__
        )

        login(self.request, user)

        messages.success(request, "Thank you!  You are now logged in.")

        return HttpResponseRedirect(reverse("orders-listing"))

    def fail_out(self):
        messages.error(
            self.request,
            mark_safe(
                'That activation key appears to be invalid.  Please contact '
                '<a href="mailto:webmaster@rxlenses.ca">our web developer</a> '
                'to see about getting this fixed.'
            )
        )
        return HttpResponseRedirect(reverse("index"))


class ConfirmationView(TemplateView):
    template_name = "users/confirm.html"


class ImpersonationView(RedirectView):

    def get_redirect_url(self, *args, **kwargs):

        backend = get_backends()[0]
        try:
            if self.request.user.is_daniel or self.request.user.is_dad:
                target_user = get_user_model().objects.get(
                    pk=kwargs.get("user_id")
                )
                target_user.backend = "%s.%s" % (
                    backend.__module__,
                    backend.__class__.__name__
                )
                if not target_user.is_daniel:
                    login(self.request, target_user)
        except Exception as e:
            logging.error(e)

        return reverse("index")
