from datetime import datetime
from dateutil.relativedelta import relativedelta
from hashlib import sha256
from random import randint

from django.contrib.gis.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

from django_extensions.db.fields import (
    CreationDateTimeField, ModificationDateTimeField)

from .managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["name", "tz"]

    TREE_GROWING_TIME_FAST = 6
    TREE_GROWING_TIME_DEFAULT = 24

    name = models.CharField(max_length=128)
    url_name = models.CharField(
        max_length=128,
        help_text="Used for your public URL (if you choose to make your "
                  "collection public)"
    )

    email = models.EmailField(unique=True)
    birth_date = models.DateField(blank=True, null=True)
    url = models.URLField(blank=True)
    tz = models.CharField("Time Zone", max_length=64, default="UTC")
    is_public = models.BooleanField(
        default=True, help_text="Is this user visible to others?")

    balance = models.PositiveIntegerField(default=0, editable=False)

    tree_location = models.PointField(geography=True, blank=True, null=True)
    tree_planted = models.DateTimeField(
        auto_now_add=True, help_text="Time at which the tree was planted")
    tree_growing_time = models.PositiveIntegerField(
        default=24,
        help_text="Time, in hours, that it takes for the tree to take root"
    )

    is_staff = models.BooleanField(
        "Staff status",
        default=False,
        help_text="Designates whether the user can log into the admin site."
    )
    is_active = models.BooleanField(
        "Active",
        default=True,
        help_text="Designates whether this user should be treated as active."
    )
    date_joined = CreationDateTimeField()
    date_modified = ModificationDateTimeField()

    objects = UserManager()

    def __str__(self):
        return self.name

    @property
    def tree_is_established(self):
        ready = self.tree_planted + relativedelta(hours=self.tree_growing_time)
        return ready < datetime.now()

    def get_absolute_url(self):
        return "/users/%s/" % self.url_name

    def get_short_name(self):
        """
        Django wants this.  Not sure why.
        """
        return self.name

    def get_username(self):
        """
        Required by Django to use as a key for authentication.
        """
        return self.email

    def get_activation_key(self):
        """
        No need to have an extra field.
        """
        return sha256("{email}-{pk}-{joined}".format(
            email=self.email,
            pk=self.pk,
            joined=self.date_joined
        )).hexdigest()

    def reset_password(self):
        password = "".join([chr(randint(97, 122)) for __ in range(10)])
        self.set_password(password)
        self.save()
        return password
