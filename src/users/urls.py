from django.conf.urls import url
from django.contrib.auth.views import logout

from .views import IndexView as Index
from .views import InventoryListingView as InventoryListing
from .views import SpiritListingView as SpiritListing
from .views import RegistrationView as Registration
from .views import ActivationView as Activation
from .views import ConfirmationView as Confirmation
from .views import ForgotView as Forgot
from .views import ImpersonationView as Impersonation

urlpatterns = [

    url(
        r"^$",
        Index.as_view(),
        name="users-index"
    ),


    # Public(ish) pages

    url(
        r"^(?P<url_name>\w+)/spirits/$",
        SpiritListing.as_view(),
        name="users-spirits"
    ),
    url(
        r"^(?P<url_name>\w+)/inventory/$",
        InventoryListing.as_view(),
        name="users-inventory"
    ),


    # Account options

    url(r"^login/$", Registration.as_view(), name="login"),
    url(r"^logout/$", logout, name="logout"),
    url(r"^thanks/$", Confirmation.as_view(), name="users-confirm"),
    url(r"^forgot/$", Forgot.as_view(), name="users-forgot"),
    url(
        r"^activate/(?P<key>.*)/$",
        Activation.as_view(),
        name="users-activation"
    ),


    # Admin options
    url(
        r"^impersonate/(?P<user_id>\d+)/$",
        Impersonation.as_view(),
        name="users-impersonation"
    )

]
