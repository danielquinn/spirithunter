from datetime import datetime

from tastypie import fields
from tastypie.exceptions import BadRequest
from tastypie.http import HttpCreated
from tastypie.resources import Resource, ModelResource

from spirits.models import Spirit

from .models import User

class UserResource(ModelResource):

    spirits = fields.ManyToManyField("spirits.api.SpiritResource", "spirits")

    class Meta:
        allowed_methods = ("get",)
        queryset = User.objects.all()
        resource_name = "users"
        excludes = [
            "email",
            "password",
            "is_active",
            "is_staff",
            "is_superuser",
            "tree_planted",
            "tree_growing_time",
            "tree_location"
        ]



class TreeResource(Resource):

    class Meta:
        allowed_methods = ("post",)
        resource_name = "tree"


    def post_list(self, request, **kwargs):

        if not request.location:
            raise BadRequest("A location must be supplied")

        if request.user.is_authenticated():

            User.objects.filter(pk=request.user.pk).update(
                tree_location=request.location,
                tree_planted=datetime.now(),
                tree_growing_time=User.TREE_GROWING_TIME_DEFAULT
            )
            Spirit.objects.filter(owner=request.user).update(
                location=request.location
            )

        return HttpCreated(request.location.geojson)


    def post_detail(self, request, **kwargs):
        raise NotImplementedError()
