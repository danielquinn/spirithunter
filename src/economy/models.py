from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.db.models.aggregates import Sum

from polymorphic.models import PolymorphicModel


class Transaction(PolymorphicModel):

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="transactions")
    user_email = models.EmailField()
    delta = models.IntegerField()
    note = models.TextField(blank=True, null=True)
    balance = models.PositiveIntegerField(
        help_text="The balance total at this point in the log.  This value "
                  "exists strictly for performance reasons when calculating "
                  "current balance."
    )
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{user}: {delta}".format(user=self.user, delta=self.delta)

    def save(self, *args, **kwargs):

        if self.user:
            if not self.user_email:
                self.user_email = self.user.email
            if not self.balance:
                self.balance = Transaction.objects.filter(
                    user=self.user
                ).aggregate(
                    total=Sum("delta")
                )["total"] or 0
                self.balance += self.delta

        r = PolymorphicModel.save(self, *args, **kwargs)

        get_user_model().objects.filter(
            pk=self.user.pk
        ).update(
            balance=self.balance
        )

        return r


class ItemPurchaseTransaction(Transaction):
    """
    Purchase of an item
    """
    item = models.ForeignKey("items.Item")
    quantity = models.PositiveIntegerField(
        help_text="Number of items purchased")


class SpoilsTransaction(Transaction):
    """
    The cash you get as the result of defeating a spirit.
    """
    pass


class AdministrativeTransaction(Transaction):
    """
    A manually executed transaction, typically as a result of a support ticket.
    """
    pass


class ForexTransaction(Transaction):
    """
    The exchange of "meatspace currency" for in-game currency
    """
    currency_value = models.PositiveIntegerField()
    credits_purchased = models.PositiveIntegerField()


class BitcoinForexTransaction(ForexTransaction):
    pass


class CreditcardForexTransaction(ForexTransaction):
    pass
