from django.contrib import admin

from .models import (
    ItemPurchaseTransaction, SpoilsTransaction, AdministrativeTransaction,
    BitcoinForexTransaction, CreditcardForexTransaction
)


class TransactionAdmin(admin.ModelAdmin):
    list_display = ["user", "delta", "balance", "created"]


class ItemPurchaseTransactionAdmin(TransactionAdmin):
    list_display = TransactionAdmin.list_display + ["item", "quantity"]


class SpoilsTransactionAdmin(TransactionAdmin):
    pass


class AdministrativeTransactionAdmin(TransactionAdmin):
    pass


class ForexTransactionAdmin(TransactionAdmin):
    list_display = TransactionAdmin.list_display + [
        "currency_value", "credits_purchased"]


class BitcoinForexTransactionAdmin(ForexTransactionAdmin):
    pass


class CreditCardForexTransactionAdmin(ForexTransactionAdmin):
    pass


admin.site.register(ItemPurchaseTransaction, ItemPurchaseTransactionAdmin)
admin.site.register(SpoilsTransaction, SpoilsTransactionAdmin)
admin.site.register(AdministrativeTransaction, AdministrativeTransactionAdmin)
admin.site.register(BitcoinForexTransaction, BitcoinForexTransactionAdmin)
admin.site.register(
    CreditcardForexTransaction, CreditCardForexTransactionAdmin)
