from rest_framework import serializers
from .models import Spirit
from geography.serializers import CountrySerializer


class SpiritSerializer(serializers.ModelSerializer):

    nationalities = CountrySerializer(many=True)

    class Meta:
        model = Spirit
        fields = (
            "id", "family", "name", "attack", "defence", "speed",
            "health_maximum", "health_current", "experience", "level",
            "origin", "location", "created", "activity", "owner", "elements",
            "facets", "nationalities"
        )
