from channels.generic.websockets import JsonWebsocketConsumer

from spirithunter.logging import LogMixin


class FightConsumer(LogMixin, JsonWebsocketConsumer):

    # Set to True if you want them, else leave out
    strict_ordering = False
    slight_ordering = False

    def connect(self, message, **kwargs):
        """
        Starts the fight
        Dump attacker & defender stats into cache
        """
        pass

    def receive(self, content, **kwargs):
        """
        Handles heartbeats
        Action is taken based on the time since the last action was taken.
        Time is measured in heartbeats
        """
        self.logger.debug(kwargs)
        self.send(content)

    def disconnect(self, message, **kwargs):
        """
        Ends the fight
        Write attacker & defender stats to the db
        Account for spoils if any
        """
        pass
