from django.contrib import admin

from .models.family import Family
from .models.level import LevelLadder
from .models.spirit import Spirit


class FamilyAdmin(admin.ModelAdmin):
    list_display = (
        "name", "stage2_level", "stage3_level", "stage4_level")


class LevelLadderAdmin(admin.ModelAdmin):
    list_display = (
        "pk", "family", "level", "attack", "defence", "health", "speed",
        "xp_required", "xp_given"
    )
    list_filter = ("family", "level")


class SpiritAdmin(admin.ModelAdmin):
    list_display = (
        "name", "attack", "defence", "speed", "experience", "level",
        "activity"
    )
    list_filter = ("owner", "level", "activity")


admin.site.register(Family, FamilyAdmin)
admin.site.register(LevelLadder, LevelLadderAdmin)
admin.site.register(Spirit, SpiritAdmin)
