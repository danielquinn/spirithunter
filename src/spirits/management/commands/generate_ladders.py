from random import randint, randrange, choice

from django.core.management.base import BaseCommand

from items.models import Item
from ...models import Family


class Command(BaseCommand):

    help = "Generates fixture ladder values"

    def handle(self, *args, **options):

        print("[")

        i = 1
        for family in Family.objects.all():

            attack = 10
            defence = 10
            health = 50
            speed = 10

            level_up_element1 = 2
            level_up_element2 = 2
            level_up_element3 = 2
            level_up_element4 = 2

            xp_required = 10
            xp_given = 10

            cash_given = 25

            stop = {
                "attack": 50,
                "defence": 50,
                "health": 50,
                "speed": 50,
                "xp_required": 50,
                "xp_given": 50,
            }

            items = ["null"] + list(Item.objects.values_list("pk", flat=True))

            for level in range(1, 100):

                attack += int(
                    float(attack)
                    * float(randrange(0, stop["attack"]))
                    / 100
                )
                defence += int(
                    float(defence)
                    * float(randrange(0, stop["defence"]))
                    / 100
                )
                health += int(
                    float(health)
                    * float(randrange(0, stop["health"]))
                    / 100
                )
                speed += int(
                    float(speed)
                    * float(randrange(0, stop["speed"]))
                    / 100
                )

                level_up_element1 *= 1 + (float(randint(0, 13)) / 100.0)
                level_up_element2 *= 1 + (float(randint(0, 13)) / 100.0)
                level_up_element3 *= 1 + (float(randint(0, 13)) / 100.0)
                level_up_element4 *= 1 + (float(randint(0, 13)) / 100.0)

                xp_required += int(
                    float(xp_required)
                    * float(randrange(0, stop["xp_required"]))
                    / 100
                )
                xp_given += int(
                    float(xp_given)
                    * float(randrange(0, stop["xp_given"]))
                    / 100
                )

                cash_given = (randrange(
                    int(float(cash_given) * 107),
                    int(float(cash_given) * 110)
                ) / 100)

                item_drop_common = choice(items)
                item_drop_rare = choice(items)

                print(
                    '  { '
                        '"pk": %(pk)s, '
                        '"model": "spirits.levelladder", '
                        '"fields": { "level": %(level)s, '
                        '"family": %(family)s, '
                        '"attack": %(attack)d, '
                        '"defence": %(defence)d, '
                        '"health": %(health)d, '
                        '"speed": %(speed)d, '
                        '"level_up_element1": %(level_up_element1)d, '
                        '"level_up_element2": %(level_up_element2)d, '
                        '"level_up_element3": %(level_up_element3)d, '
                        '"level_up_element4": %(level_up_element4)d, '
                        '"xp_required": %(xp_required)d, '
                        '"xp_given": %(xp_given)d, '
                        '"cash_given": %(cash_given)d, '
                        '"item_drop_common": %(item_drop_common)s, '
                        '"item_drop_rare": %(item_drop_rare)s '
                    '} },' % {
                        "pk": i,
                        "family": family.pk,
                        "level": level,
                        "attack": attack,
                        "defence": defence,
                        "health": health,
                        "speed": speed,
                        "level_up_element1": level_up_element1,
                        "level_up_element2": level_up_element2,
                        "level_up_element3": level_up_element3,
                        "level_up_element4": level_up_element4,
                        "xp_required": xp_required,
                        "xp_given": xp_given,
                        "cash_given": cash_given,
                        "item_drop_common": item_drop_common,
                        "item_drop_rare": item_drop_rare
                    }
                )

                i += 1
                for key, value in stop.items():
                    if value > 5:
                        stop[key] = int(value * 0.99)

        print("]")
