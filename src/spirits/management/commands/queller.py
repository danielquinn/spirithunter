from django.core.management.base import BaseCommand
from datetime import datetime
from ...models import Spirit


class Command(BaseCommand):

    help = "Removes expired spirits from the database"

    def handle(self, *args, **options):

        Spirit.objects.filter(
            expires__lt=datetime.utcnow(),
            owner__isnll=True
        ).delete()
