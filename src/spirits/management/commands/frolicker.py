from django.core.management.base import BaseCommand
from django.db import connection

from ...models import Spirit


class Command(BaseCommand):
    """
    The idea is to achieve full strength in `FULL_HEALTH_HOURS` hours.
    """

    FULL_HEALTH_HOURS = 8  # They say that's what makes for a good sleep right?

    help = "Regenerates the health of spirits that are currently frolicking"

    def handle(self, *args, **options):

        divisor = self.FULL_HEALTH_HOURS * 60 / 5
        query = """
            UPDATE
              spirits_spirit
            SET
              health_current = LEAST(
                health_current + GREATEST(ROUND((health_maximum / {divisor:d})), 1),
                health_maximum
              )
            WHERE
              activity = {frolicking:d} AND
              health_current < health_maximum;
        """.format(
            divisor=divisor,
            frolicking=Spirit.ACTIVITY_FROLIC
        )

        cursor = connection.cursor()
        cursor.execute(query)
