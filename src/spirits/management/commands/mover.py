from django.core.management.base import BaseCommand
from django.db import connection

from ...models import Spirit


class Command(BaseCommand):
    """
    https://stackoverflow.com/questions/24870747/need-a-query-to-change-a-geometry-column-value-to-a-new-one-based-on-the-old-one/24872963
    """

    help = "Moves wandering spirits."

    def handle(self, *args, **options):

        query = """
            UPDATE
                spirits_spirit
            SET
                location = ST_TRANSLATE(
                    location::geometry,
                    CASE
                        WHEN {baseline} % 10 = 0 THEN random() * -0.001
                        WHEN {baseline} % 9  = 0 THEN random() *  0.001
                        WHEN {baseline} % 8  = 0 THEN random() * -0.001
                        WHEN {baseline} % 7  = 0 THEN random() *  0.001
                        WHEN {baseline} % 6  = 0 THEN random() * -0.001
                        WHEN {baseline} % 5  = 0 THEN random() *  0.001
                        WHEN {baseline} % 4  = 0 THEN random() * -0.001
                        WHEN {baseline} % 3  = 0 THEN random() *  0.001
                        WHEN {baseline} % 2  = 0 THEN random() * -0.001
                        ELSE random() * 0.001
                    END,
                    CASE
                        WHEN {baseline} % 10 = 0 THEN random() *  0.001
                        WHEN {baseline} % 9  = 0 THEN random() * -0.001
                        WHEN {baseline} % 8  = 0 THEN random() *  0.001
                        WHEN {baseline} % 7  = 0 THEN random() * -0.001
                        WHEN {baseline} % 6  = 0 THEN random() *  0.001
                        WHEN {baseline} % 5  = 0 THEN random() * -0.001
                        WHEN {baseline} % 4  = 0 THEN random() *  0.001
                        WHEN {baseline} % 3  = 0 THEN random() * -0.001
                        WHEN {baseline} % 2  = 0 THEN random() *  0.001
                        ELSE random() * -0.001
                    END
                )
            WHERE
              activity = {activity}
        """.format(
            activity=Spirit.ACTIVITY_WANDER,
            baseline="(ST_DISTANCE('0101000020E610000000000000000000000000000000000000'::GEOGRAPHY, location)::FLOAT * 10000)::BIGINT"
        )

        cursor = connection.cursor()
        cursor.execute(query)
