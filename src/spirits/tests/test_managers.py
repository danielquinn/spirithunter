import os

import pytest

from unittest import mock

from django.contrib.gis.geos import fromstr
from django.core import management
from django.core.cache import cache
from django.test import TestCase

from aspects.arbiters.celestial import CelestialArbiter
from aspects.arbiters.time import TimeArbiter
from aspects.models.facet import Facet
from aspects.models.elements import Element
from geography.models import Country

from .factories import SpiritFactory, FamilyFactory

from ..models.spirit import Spirit
from ..models.family import Family
from ..models.level import LevelLadder


@pytest.mark.django_db
class SpiritManagerTest(TestCase):

    FIXTURES = {
        "aspects": os.path.join(
            os.path.dirname(__file__),
            "..",
            "..",
            "aspects",
            "fixtures",
            "initial_data.json"
        ),
        "items": os.path.join(
            os.path.dirname(__file__),
            "..",
            "..",
            "items",
            "fixtures",
            "initial_data.json"
        ),
        "families": os.path.join(
            os.path.dirname(__file__),
            "..",
            "fixtures",
            "families.json"
        ),
        "ladders": os.path.join(
            os.path.dirname(__file__),
            "..",
            "fixtures",
            "ladders.json"
        ),
        "geography": os.path.join(
            os.path.dirname(__file__),
            "..",
            "..",
            "geography",
            "fixtures",
            "initial_data.json"
        ),
    }

    @classmethod
    def setUpClass(cls):

        super(SpiritManagerTest, cls).setUpClass()

        f = cls.FIXTURES
        management.call_command("loaddata", f["items"], verbosity=0)
        management.call_command("loaddata", f["families"], verbosity=0)
        management.call_command("loaddata", f["ladders"], verbosity=0)
        management.call_command("loaddata", f["aspects"], verbosity=0)
        management.call_command("loaddata", f["geography"], verbosity=0)

    def tearDown(self):
        """
        Apparently, Django's test framework doesn't clear the cache between
        tests: https://code.djangoproject.com/ticket/11505
        """
        cache.clear()

    def test_create_random_creates_anything(self):
        self.assertEqual(Spirit.objects.all().count(), 0)
        Spirit.objects.create_random()
        self.assertEqual(Spirit.objects.all().count(), 1)

    def test_create_random_creates_n_spirits(self):
        self.assertEqual(Spirit.objects.all().count(), 0)
        Spirit.objects.create_random(total=10)
        self.assertEqual(Spirit.objects.all().count(), 10)

    def test__atomic_random(self):

        all_countries = Country.objects.all()

        tests = (
            (1, 2, (80, -18), (all_countries[1],)),
            (8, 10, (16, 123), (all_countries[8],)),
            (8, 10, (67, -63), (all_countries[10], all_countries[12])),
        )

        for low, high, (lat, lng), countries in tests:

            spirit = Spirit.objects._atomic_random(
                low,
                high,
                fromstr("POINT({lng} {lat})".format(lat=lat, lng=lng)),
                countries
            )

            self.assertIn(spirit.level, range(low, high + 6))
            self.assertEqual(spirit.location.get_x(), float(lng))
            self.assertEqual(spirit.location.get_y(), float(lat))

            for country in countries:
                self.assertIn(country, spirit.nationalities.all())

    def test__create_spirit(self):

        ladder = LevelLadder.objects.get(pk=1)
        location = fromstr("POINT({lng} {lat})".format(lat=1, lng=0))

        self.assertEqual(Spirit.objects.all().count(), 0)
        Spirit.objects._create_spirit(ladder, location)
        self.assertEqual(Spirit.objects.all().count(), 1)

        spirit = Spirit.objects.all()[0]
        self.assertEqual(spirit.health_maximum, ladder.health)
        self.assertEqual(spirit.health_current, ladder.health)
        self.assertEqual(spirit.experience, ladder.xp_required)
        self.assertEqual(spirit.location.get_x(), 0.0)
        self.assertEqual(spirit.location.get_y(), 1.0)
        self.assertEqual(spirit.location, spirit.origin)
        for attr in ("family", "level", "attack", "defence", "speed"):
            self.assertEqual(getattr(spirit, attr), getattr(ladder, attr))

    @mock.patch.object(Spirit.objects, "_generate_aspects")
    def test__set_facets(self, mocked_generate_aspects):

        test_facets = Facet.objects.all()[:2]
        mocked_generate_aspects.return_value = test_facets

        spirit = SpiritFactory.create()
        Spirit.objects._set_facets(spirit, None)
        actual_facets = spirit.facets.all()
        self.assertEqual(set(test_facets), set(actual_facets))

    @mock.patch.object(Spirit.objects, "_generate_aspects")
    @mock.patch("spirits.managers.randint")
    def test__set_nationalities_tourist(self, randint, _generate_aspects):

        tourist_nationality = Country.objects.get(country="NL")
        aspect_nationalities = Country.objects.filter(country__in=("CA", "GR"))

        randint.return_value = 1  # Tourist
        _generate_aspects.return_value = aspect_nationalities

        with mock.patch("spirits.managers.SpiritManager._get_tourist") as m:
            m.return_value = tourist_nationality
            spirit = SpiritFactory.create()
            Spirit.objects._set_nationalities(spirit, None, None)

        actual_nationalities = list(spirit.nationalities.all())

        self.assertEqual(
            set([tourist_nationality] + list(aspect_nationalities)),
            set(actual_nationalities)
        )

    @mock.patch.object(Spirit.objects, "_generate_aspects")
    @mock.patch("spirits.managers.randint")
    def test__set_nationalities_tourist(self, randint, _generate_aspects):

        geo_nationalities = Country.objects.filter(country="CA")
        aspect_nationalities = Country.objects.filter(country="GR")

        randint.return_value = 0  # Not a tourist
        _generate_aspects.return_value = aspect_nationalities
        location = fromstr("POINT(-70 47)")  # Canada

        spirit = SpiritFactory.create()
        Spirit.objects._set_nationalities(spirit, None, location)

        actual_nationalities = list(spirit.nationalities.all())

        self.assertEqual(
            set(list(aspect_nationalities) + list(geo_nationalities)),
            set(actual_nationalities)
        )

    def test__get_tourist(self):

        control_list = Country.objects.filter(country__in=["CA", "GR", "NL"])

        tourist = Spirit.objects._get_tourist(control_list)

        self.assertIsInstance(tourist, Country)
        self.assertIn(tourist, control_list)

    def test__imbue_with_elements(self):

        spirit = SpiritFactory.create()
        elements = Element.objects.filter(pk__lt=3)
        level = LevelLadder.objects.get(pk=3)

        mock_args = [spirit.elements.through.objects, "create"]
        with mock.patch.object(*mock_args) as mocked:

            Spirit.objects._imbue_with_elements(spirit, elements, level, True)

            self.assertTrue(mocked.called)
            self.assertEqual(mocked.call_count, 2)

            kwargs = mocked.call_args[1]
            self.assertIsInstance(kwargs["spirit"], Spirit)
            self.assertIsInstance(kwargs["element"], Element)
            self.assertIsInstance(kwargs["strength"], int)
            self.assertTrue(mocked.call_args[1]["strength"] > 0)

            Spirit.objects._imbue_with_elements(spirit, elements, level, False)

            self.assertTrue(mocked.call_args[1]["strength"] < 0)

    def test__get_family(self):
        """
        Presently, all _get_family() does is grab a random family and return
        it, so this test is pretty boring.
        """
        self.assertIn(Spirit.objects._get_family(), Family.objects.all())

    def test__get_ladder_from_family(self):

        family = Family.objects.all()[0]

        for low, high in ((1, 3), (3, 5), (7, 8), (6, 9), (1, 2), (9, 9)):

            potential_ladders = LevelLadder.objects.filter(
                level__gte=low, level__lte=high + 5)

            with mock.patch("spirits.managers.LevelLadder.MAX_LEVEL", high):
                ladder = Spirit.objects._get_ladder_from_family(
                    family, low, high)

            self.assertIn(ladder, potential_ladders)

    def test__get_arbitration(self):

        class FakeArbiter(dict):

            @staticmethod
            def successful():
                return True

            @property
            def result(self):
                return self

        def mocked_delay(kind, centre):

            assert centre == (0.12, 0.79)

            r = FakeArbiter(elements=[], facets=[], nationalities=[])

            if kind == "celestial":
                r["elements"].append(
                    (Element.ELEMENT_DAY, CelestialArbiter.WEIGHT_DAY)
                )
            if kind == "time":
                r["nationalities"].append(
                    ("CA", TimeArbiter.WEIGHT_NATIONAL_DAY)
                )

            return r

        with mock.patch("spirits.managers.arbitrate.delay", mocked_delay):
            self.assertEqual(
                Spirit.objects._get_arbitration((0.123, 0.789)),
                {
                    "elements": {
                        (Element.ELEMENT_DAY, CelestialArbiter.WEIGHT_DAY)
                    },
                    "facets": set(),
                    "nationalities": {
                        ("CA", TimeArbiter.WEIGHT_NATIONAL_DAY)
                    }
                }
            )

    def test__get_arbitration_caching(self):
        with mock.patch("spirits.managers.arbitrate.delay") as mocked:
            Spirit.objects._get_arbitration((0.123, 0.789))
            self.assertEqual(mocked.call_count, 4)
            Spirit.objects._get_arbitration((0.123, 0.789))
            self.assertEqual(mocked.call_count, 4)
            Spirit.objects._get_arbitration((0.123, 3.789))
            self.assertEqual(mocked.call_count, 8)

    def test__generate_aspects(self):

        day = Element.objects.get(pk=Element.ELEMENT_DAY)
        nature = Element.objects.get(pk=Element.ELEMENT_NATURE)
        windy = Element.objects.get(pk=Element.ELEMENT_WIND)
        pi = Facet.objects.get(pk=Facet.FACET_PI)
        mars = Facet.objects.get(pk=Facet.FACET_MARS)

        pairings = ((day.pk, 3), (nature.pk, 7), (windy.pk, 9))
        attributes = Spirit.objects._generate_aspects(pairings, Element, 3)

        self.assertEqual(attributes.count(), 3)
        self.assertIn(day, attributes)
        self.assertIn(nature, attributes)
        self.assertIn(windy, attributes)

        pairings = ((pi.pk, 3), (mars.pk, 7))
        attributes = Spirit.objects._generate_aspects(pairings, Facet, 3)

        self.assertEqual(attributes.count(), 2)
        self.assertIn(pi, attributes)
        self.assertIn(mars, attributes)

    def test__generate_aspects_no_pairings(self):
        self.assertEqual(
            Spirit.objects._generate_aspects((), Element, 3).count(), 0)

    def test__get_total_elements(self):

        family = FamilyFactory(stage2_level=5)
        self.assertEqual(Spirit.objects._get_total_elements(4, family), 1)
        self.assertEqual(Spirit.objects._get_total_elements(5, family), 2)
        self.assertEqual(Spirit.objects._get_total_elements(6, family), 2)

        family = FamilyFactory(stage2_level=5, stage3_level=7)
        self.assertEqual(Spirit.objects._get_total_elements(6, family), 2)
        self.assertEqual(Spirit.objects._get_total_elements(7, family), 3)
        self.assertEqual(Spirit.objects._get_total_elements(8, family), 3)

        family = FamilyFactory(stage2_level=5, stage3_level=7, stage4_level=9)
        self.assertEqual(Spirit.objects._get_total_elements(8, family), 3)
        self.assertEqual(Spirit.objects._get_total_elements(9, family), 4)
        self.assertEqual(Spirit.objects._get_total_elements(10, family), 4)

    def test__get_negative_elements(self):
        positives = (
            (1, 2, 3, 4),
            (5, 6, 7, 8),
            (1, 7),
            (2, 4, 6, 8)
        )
        for i in range(20):  # Repeat to account for randomness
            for positive_set in positives:
                negatives = Spirit.objects._get_negative_elements(
                    Element.objects.filter(pk__in=positive_set))
                self.assertGreaterEqual(len(negatives), 0)
                self.assertLessEqual(len(negatives), len(positive_set) + 1)
                for element in negatives:
                    self.assertNotIn(element.pk, positive_set)
