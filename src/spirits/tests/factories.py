import factory

from django.contrib.gis.geos import fromstr

from spirits.models import Spirit, Family


class FamilyFactory(factory.DjangoModelFactory):

    class Meta(object):
        model = Family

    name = "Name"
    slug = "name"
    description = "description"
    stage2_level = 20
    stage3_level = 40
    stage4_level = 60


class SpiritFactory(factory.DjangoModelFactory):

    class Meta(object):
        model = Spirit

    family = factory.SubFactory(FamilyFactory)
    name = "Name"
    attack = 0
    defence = 0
    speed = 0
    health_maximum = 0
    health_current = 0
    origin = fromstr("POINT({lng} {lat})".format(lat=0, lng=0))
    location = fromstr("POINT({lng} {lat})".format(lat=0, lng=0))
    level = 1
