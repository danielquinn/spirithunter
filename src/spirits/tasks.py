from celery.utils.log import get_task_logger

from aspects.arbiters import (
    CelestialArbiter, TimeArbiter, TerrainArbiter, WeatherArbiter)
from spirithunter.celery import app

logger = get_task_logger(__name__)

ARBITRATION_KEY = {
    "celestial": CelestialArbiter,
    "time": TimeArbiter,
    "terrain": TerrainArbiter,
    "weather": WeatherArbiter
}


@app.task
def arbitrate(klass, centre):

    logger.debug(
        "Determining arbitration for %s at (%s,%s)",
        klass, centre[0], centre[1]
    )

    try:
        return ARBITRATION_KEY[klass](centre[0], centre[1]).get_results()
    except Exception as e:
        logger.error(e)
        return {
            "elements": [],
            "facets": [],
            "nationalities": []
        }
