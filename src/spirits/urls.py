from django.conf.urls import url

from .views import FinderView as Finder
from .views import DetailView as Detail

urlpatterns = [
    url(r"^finder/$", Finder.as_view(), name="spirits-finder"),
    url(r"^(?P<pk>\d+)/$", Detail.as_view(), name="spirits-detail"),
]
