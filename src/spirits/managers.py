from datetime import datetime
from dateutil.relativedelta import relativedelta
from itertools import chain, repeat
from random import randint, choice
from time import sleep

from django.contrib.gis.db import models
from django.contrib.gis.geos import fromstr
from django.core.cache import cache
from django.db import transaction
from django.utils.timezone import make_aware

from aspects.models import Element, Facet
from aspects.arbiters import (
    CelestialArbiter, TerrainArbiter, TimeArbiter, WeatherArbiter)
from geography.models import Country
from spirithunter.logging import LogMixin

from .models.level import LevelLadder
from .models.family import Family
from .tasks import arbitrate


class SpiritManager(LogMixin, models.GeoManager):

    ELEMENT_COMBOS = (
        (Element.ELEMENT_DAY, CelestialArbiter.WEIGHT_DAY),
        (Element.ELEMENT_NIGHT, CelestialArbiter.WEIGHT_NIGHT),
        (Element.ELEMENT_MOON_NEW, CelestialArbiter.WEIGHT_MOON_NEW),
        (Element.ELEMENT_MOON_FULL, CelestialArbiter.WEIGHT_MOON_FULL),
        (Element.ELEMENT_WATER, TerrainArbiter.WEIGHT_WATER),
        (Element.ELEMENT_NATURE, TerrainArbiter.WEIGHT_NATURE),
        (Element.ELEMENT_HOT, WeatherArbiter.WEIGHT_HOT),
        (Element.ELEMENT_COLD, WeatherArbiter.WEIGHT_COLD),
        (Element.ELEMENT_WIND, WeatherArbiter.WEIGHT_WIND)
    )

    FACET_COMBOS = (
        (Facet.FACET_MERCURY, CelestialArbiter.WEIGHT_MERCURY),
        (Facet.FACET_VENUS, CelestialArbiter.WEIGHT_VENUS),
        (Facet.FACET_MARS, CelestialArbiter.WEIGHT_MARS),
        (Facet.FACET_JUPITER, CelestialArbiter.WEIGHT_JUPITER),
        (Facet.FACET_SATURN, CelestialArbiter.WEIGHT_SATURN),
        (Facet.FACET_URANUS, CelestialArbiter.WEIGHT_URANUS),
        (Facet.FACET_NEPTUNE, CelestialArbiter.WEIGHT_NEPTUNE),
        (Facet.FACET_PI, TimeArbiter.WEIGHT_PI),
        (Facet.FACET_STARWARS, TimeArbiter.WEIGHT_STARWARS),
        (Facet.FACET_TOWEL, TimeArbiter.WEIGHT_TOWEL),
    )

    CACHE_KEY = "spirit-manager:%s,%s"
    CACHE_TIME = 3 * 60 * 60

    def create_random(self, lat=0, lng=0, level_low=1, level_high=3, total=1):
        """
        Create a spirit with randomised attributes.  Typically you'll use this
        for assigning a "starter deck" to a new user.
        """

        location = fromstr("POINT({lng} {lat})".format(lat=lat, lng=lng))
        countries = Country.objects.filter(
            exclusive_economic_zone__geometry__contains=location)

        r = []
        with transaction.atomic():
            for i in range(0, total):
                r.append(self._atomic_random(
                    level_low, level_high, location, countries))

        return r

    def _atomic_random(self, level_low, level_high, location, countries):
        """
        Create and return a single spirit.  There's very little thinking here,
        as the country is pre-determined.  We don't even use the location for
        anything other than getting the origin.

        :param level_low: int The lowest level the spirit can be
        :param level_high: int The highest level the spirit can be
        :param location: point The origin of the spirit
        :param countries: queryset A list of countries the spirit is from.
                                   Typically this is only ever 1 place.

        :return: A spirit
        """

        # Don't let us overshoot the max level available
        level_high = min(LevelLadder.MAX_LEVEL, level_high)

        family = self._get_family()
        ladder = self._get_ladder_from_family(family, level_low, level_high)
        spirit = self._create_spirit(ladder, location)

        total_elements = self._get_total_elements(ladder.level, family)

        positive_elements = self._generate_aspects(
            self.ELEMENT_COMBOS, Element, total_elements)
        negative_elements = self._get_negative_elements(positive_elements)

        self._imbue_with_elements(
            spirit, positive_elements, ladder, is_positive=True)
        self._imbue_with_elements(
            spirit, negative_elements, ladder, is_positive=False)

        facets = set(self._generate_aspects(
            self.FACET_COMBOS, Facet, randint(0, 2)))
        spirit.facets.add(*facets)

        spirit.nationalities.add(*countries)

        return spirit

    def create_for_environment(self, centre, target,
                               level_low=1, level_high=1):
        """
        Good testing coordinates:
          nature: 59.775627,-115.333707
          water: 0,0

        Steps:
          1. Determine the family
          2. Determine the elements present
          3. Based on the levels of the user, create a spirit with the
             specified family and a number of elements warranted for a spirit
             of that level
        """

        self.logger.debug("Create for environment: start")

        family = self._get_family()
        ladder = self._get_ladder_from_family(family, level_low, level_high)
        location = fromstr("POINT({lng} {lat})".format(
            lat=target[0], lng=target[1]))

        total_elements = self._get_total_elements(ladder.level, family)
        arbitration = self._get_arbitration(centre)
        positive_elements = self._generate_aspects(
            arbitration["elements"], Element, total_elements)
        negative_elements = self._get_negative_elements(positive_elements)

        if not positive_elements:
            self.logger.error("No elements were found for %s,%s", *centre)
            return

        self.logger.debug("Creating spirit: start")

        spirit = self._create_spirit(ladder, location)

        self._imbue_with_elements(
            spirit, positive_elements, ladder, is_positive=True)
        self._imbue_with_elements(
            spirit, negative_elements, ladder, is_positive=False)

        self._set_facets(spirit, arbitration["facets"])
        self._set_nationalities(spirit, arbitration["nationalities"], location)

        self.logger.debug("Creating spirit: stop")

    def _create_spirit(self, ladder, location):
        spirit = self.create(
            family=ladder.family,
            attack=ladder.attack,
            defence=ladder.defence,
            speed=ladder.speed,
            health_maximum=ladder.health,
            health_current=ladder.health,
            experience=ladder.xp_required,
            level=ladder.level,
            location=location,
            expires=make_aware(datetime.now() + relativedelta(hours=5)),
            activity=self.model.ACTIVITY_WANDER,
            origin=location
        )
        self.logger.debug("Spirit created: %s", spirit)
        return spirit

    def _set_facets(self, spirit, arbitration):
        facets = set(self._generate_aspects(arbitration, Facet, randint(0, 2)))
        self.logger.debug("Applying facets: %s", facets)
        spirit.facets.add(*facets)

    def _set_nationalities(self, spirit, arbitration, location):
        """
        Nationalities come from a few places:

        * Arbitration from things like national days via the TimeArbiter
        * Physical location via a query to the Country model
        * Tourism via the 1/n case where we assign a non-local nationality.

        :param spirit: A spirit object
        :param arbitration: A tuple of tuples: ((country, weight),...)
        :param location: a Point object we use to query the Country model
        """

        nationalities = set(
            self._generate_aspects(arbitration, Country, randint(0, 2)))

        # Tourist vs. local
        countries = Country.objects.exclude(country__in=nationalities)
        if randint(1, Country.TOURISM_CHANCE) == 1:
            nationalities.add(self._get_tourist(countries))
        else:
            nationalities.update(set(countries.filter(
                exclusive_economic_zone__geometry__contains=location
            )))

        self.logger.debug("Applying nationalities: %s", nationalities)

        spirit.nationalities.add(*nationalities)

    def _get_tourist(self, countries):
        """
        Using a set of countries (that already exclude arbitrated countries),
        get a "tourist" nationality to attach to the spirit.
        :param countries: A set of countries (not a queryset)
        :return: A set containing a single Country object in it
        """

        # TODO: Make this a little more interesting:
        # if randint(1, 100) == 1:
        #     return {countries.filter(
        #         is_tourist=COUNTRY_TOURIST_RARE
        #     ).order_by("?").first()}
        # elif randint(1, 50) == 1:
        #     return {countries.filter(
        #         is_tourist=COUNTRY_TOURIST_UNCOMMON
        #     ).order_by("?").first()}
        # else:
        #     return {countries.filter(
        #         is_tourist=COUNTRY_TOURIST_COMMON
        #     ).order_by("?").first()}

        self.logger.debug("Tourist nationality selected")

        return countries.order_by("?").first()

    @staticmethod
    def _imbue_with_elements(spirit, elements, ladder, is_positive):
        """
        Attaches elements to a spirit, with a little randomised sauce.

        :param spirit: A spirit object
        :param elements: A list of Element objects
        :param ladder: A ladder object
        :param is_positive: A boolean determining if this is a positive
                            application or a negative one.
        """

        multiplier = 1 if is_positive else -1
        for element in elements:
            spirit.elements.through.objects.create(
                spirit=spirit,
                element=element,
                strength=round(choice(range(
                    int(float(min(ladder.attack, ladder.defence)) * 0.7),
                    int(float(max(ladder.attack, ladder.defence)) * 1.3),
                ))) * multiplier
            )

    def _get_family(self):
        """
        We might want to make this more elaborate, making some families
        available only under certain conditions
        """
        family = Family.objects.order_by("?").first()
        self.logger.debug("Family acquired: %s", family)
        return family

    def _get_ladder_from_family(self, family, low, high):
        high = min(LevelLadder.MAX_LEVEL, high + 5)
        ladder = LevelLadder.objects.get(
            family=family, level=randint(low, high))
        self.logger.debug("Ladder acquired: %s", ladder)
        return ladder

    def _get_arbitration(self, centre):
        """
        Handles the multi-threaded nature of fetching a bunch of arbitration
        data from concurrent APIs.

        Arbitration is the act of each arbiter returning a list of aspects that
        match the current situation.  For example, the celestial arbiter
        returns:

          {
            elements: ((DAY, WEIGHT),),
            facets: ((MERCURY, WEIGHT),)
            nationalities: ()
          }

        for a coordinate near water during the daytime when mercury is above
        the horizon.  Similarly, the time arbiter will return:

          {
            elements: (),
            facets: (),
            nationalities: ((CANADA, WEIGHT),)
          }

        on July 1st.
        """

        self.logger.debug("  Arbitration start")

        rounded_centre = (round(centre[0], 2), round(centre[1], 2))

        cache_key = self.CACHE_KEY % rounded_centre

        r = cache.get(cache_key)

        if not r:

            self.logger.debug("    Arbitration not cached!")

            celestial_arbitration = arbitrate.delay(
                "celestial", rounded_centre)
            terrain_arbitration = arbitrate.delay("terrain", rounded_centre)
            weather_arbitration = arbitrate.delay("weather", rounded_centre)
            time_arbitration = arbitrate.delay("time", rounded_centre)

            while not celestial_arbitration.successful():
                while not terrain_arbitration.successful():
                    while not weather_arbitration.successful():
                        while not time_arbitration.successful():
                            sleep(0.01)

            self.logger.debug("    Arbiters ready")

            r = {
                "elements": set(chain(
                    celestial_arbitration.result["elements"],
                    terrain_arbitration.result["elements"],
                    weather_arbitration.result["elements"],
                    time_arbitration.result["elements"],
                )),
                "facets": set(chain(
                    celestial_arbitration.result["facets"],
                    terrain_arbitration.result["facets"],
                    weather_arbitration.result["facets"],
                    time_arbitration.result["facets"]
                )),
                "nationalities": set(chain(
                    celestial_arbitration.result["nationalities"],
                    terrain_arbitration.result["nationalities"],
                    weather_arbitration.result["nationalities"],
                    time_arbitration.result["nationalities"]
                )),
            }

            self.logger.debug("    Arbitration defined")

            cache.set(cache_key, r, self.CACHE_TIME)

        self.logger.debug("  Arbitration complete")

        return r

    def _generate_aspects(self, pairings, klass, total_aspects):
        """
        Return a list of `klass` instances spawned from `pairings` totalling
        `total_aspects`.

        :param pairings: A list of tuples in the format:
          [(int pk, int weight),...]
        :param klass: One of Element, Facet, or Country
        :param total_aspects: An integer typically not larger than 4
        :return: Queryset
        """

        self.logger.debug("_generate_aspects: start")

        aspect_choices = []
        for aspect, weight in pairings:
            aspect_choices += list(repeat(aspect, weight))

        if not aspect_choices:
            return klass.objects.none()

        pks = set()
        while len(pks) < min(total_aspects, len(pairings)):
            pks.add(choice(aspect_choices))

        self.logger.debug("_get_total_elements: stop (%s)", pks)

        return klass.objects.filter(pk__in=pks)

    def _get_total_elements(self, level, family):
        """
        The number of elements assigned to a spirit is based on which stage
        in its life it's in.
        """

        self.logger.debug("_get_total_elements(%s, %s)", level, family)

        if level >= family.stage4_level:
            return 4
        elif level >= family.stage3_level:
            return 3
        elif level >= family.stage2_level:
            return 2

        return 1

    def _get_negative_elements(self, positive_elements):
        """
        Based on a list of positive elements already being assigned, we return
        `n` random elements where `n` =~ the total number of positive elements.
        """

        r = []

        positive_ids = [e.pk for e in positive_elements]
        negative_ids = [e[0] for e in self.ELEMENT_COMBOS]

        potential_elements = list(Element.objects.filter(
            pk__in=negative_ids
        ).exclude(
            pk__in=positive_ids
        ).order_by("?"))

        for i in range(0, randint(0, len(positive_elements) + 1)):
            r.append(potential_elements.pop())

        self.logger.debug("Negative elements: %s", r)

        return r
