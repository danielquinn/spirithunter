from django.apps import AppConfig


class SpiritsConfig(AppConfig):
    name = "spirits"
