import random

from math import sin, cos

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView

from rest_framework.exceptions import ValidationError
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny

from aspects.models import Element
from spirithunter.logging import LogMixin

from .models.spirit import Spirit
from .serializers import SpiritSerializer


class SpiritViewSet(ModelViewSet):
    queryset = Spirit.objects.all()
    serializer_class = SpiritSerializer
    http_method_names = ("get",)


class SpiritFinderViewSet(LogMixin, ModelViewSet):
    """
    Currently, this is designed to work via the REST API, but could (should?)
    work with websockets to do cool stuff like auto-initiating a fight.
    """

    SPIRITS_TO_GENERATE = 5
    SPAWN_RADIUS = 50

    serializer_class = SpiritSerializer
    permission_classes = (AllowAny,)
    http_method_names = ("get",)
    queryset = Spirit.objects.none()

    def get_queryset(self):

        lat, lng = (self.request.location.y, self.request.location.x)

        if lat > 80 or lat < -80:
            raise ValidationError("Invalid lat value: %s" % lat)

        if lng > 180 or lng < -180:
            raise ValidationError("Invalid lng value: %s" % lng)

        low, high = self._get_levels()

        spirits = list(Spirit.objects.filter(
            activity=Spirit.ACTIVITY_WANDER,
            health_current__gt=0,
            location__distance_lte=(self.request.location, self.SPAWN_RADIUS)
        ))

        while len(spirits) < self.SPIRITS_TO_GENERATE:
            spirits.append(self._generate_spirit(lat, lng, low, high))

        return spirits

    def _get_levels(self):

        low, high = 1, 1
        if self.request.user.is_authenticated():
            spirit_levels = sorted(self.request.user.spirits.filter(
                activity=Spirit.ACTIVITY_JARRED
            ).values_list("level", flat=True))
            if spirit_levels:
                low, high = spirit_levels[0], spirit_levels[-1]

        return low, high

    def _get_nearby_location(self, lat, lng):
        """
        This is magic that I honestly don't understand, but basically it takes
        a centre point and gives you a random location near to that point.

        :param lat: Degrees latitude
        :param lng: Degrees longitude
        :return: A new lat,lng coordinate
        """

        centre_x = float(lat)
        centre_y = float(lng)
        r = random.uniform(0, self.SPAWN_RADIUS)
        a = random.uniform(0, 360)

        return (
            centre_x,
            centre_y,
            centre_x + ((r * cos(a)) / settings.M_LNG),
            centre_y + ((r * sin(a)) / settings.M_LAT)
        )

    def _generate_spirit(self, lat, lng, low, high):

        centre_x, centre_y, target_x, target_y = self._get_nearby_location(
            lat, lng)

        self.logger.debug("Creating a spirit at %s,%s", target_x, target_y)

        return Spirit.objects.create_for_environment(
            centre=(centre_x, centre_y),
            target=(target_x, target_y),
            level_low=low,
            level_high=high
        )


class FinderView(LogMixin, LoginRequiredMixin, TemplateView):

    template_name = "spirits/base.html"

    def get_context_data(self, **kwargs):
        self.logger.debug("get_context_data: start")
        context = TemplateView.get_context_data(self, **kwargs)
        context.update({
            "available_spirits": Spirit.objects.filter(
                owner=self.request.user,
                activity=Spirit.ACTIVITY_JARRED,
                health_current__gt=0
            ).prefetch_related("elements", "facets", "nationalities")
        })
        self.logger.debug("get_context_data: stop")
        return context


class DetailView(TemplateView):

    template_name = "spirits/detail.html"

    def get_context_data(self, **kwargs):

        context = TemplateView.get_context_data(self, **kwargs)

        spirit = get_object_or_404(Spirit, pk=kwargs["pk"])
        permissions = {
            "is_administrator": spirit.owner == self.request.user
        }

        context.update({
            "spirit": spirit,
            "elements": {
                "positive": Element.objects.filter(
                    applications__spirit=spirit,
                    applications__strength__gte=0
                ),
                "negative": Element.objects.filter(
                    applications__spirit=spirit,
                    applications__strength__lt=0
                ),
            },
            "permissions": permissions
        })

        return context
