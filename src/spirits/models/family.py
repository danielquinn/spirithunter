from django.contrib.gis.db import models

from django_extensions.db.fields import AutoSlugField


class Family(models.Model):

    class Meta:
        app_label = "spirits"
        verbose_name_plural = "families"

    name = models.CharField(max_length=128)
    slug = AutoSlugField(populate_from="name")
    description = models.TextField()
    stage2_level = models.PositiveIntegerField()
    stage3_level = models.PositiveIntegerField()
    stage4_level = models.PositiveIntegerField()

    def __str__(self):
        return self.name
