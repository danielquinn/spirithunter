from django.contrib.gis.db import models

from .family import Family


class LevelLadder(models.Model):
    """
    Map of levels to families and the xp required to achieve said level or
    given when defeated.
    """

    class Meta:
        unique_together = ("level", "family")
        app_label = "spirits"
        ordering = ("family", "level")

    MAX_LEVEL = 99

    level = models.PositiveIntegerField()
    family = models.ForeignKey(Family, related_name="ladder")
    attack = models.PositiveIntegerField(
        help_text="Base value for all spirits of this family and level")
    defence = models.PositiveIntegerField(
        help_text="Base value for all spirits of this family and level")
    health = models.PositiveIntegerField(
        help_text="Base value for all spirits of this family and level")
    speed = models.PositiveIntegerField(
        help_text="Base value for all spirits of this family and level")

    level_up_element1 = models.PositiveIntegerField(
        help_text="This isn't worked out yet")
    level_up_element2 = models.PositiveIntegerField(
        help_text="This isn't worked out yet")
    level_up_element3 = models.PositiveIntegerField(
        help_text="This isn't worked out yet")
    level_up_element4 = models.PositiveIntegerField(
        help_text="This isn't worked out yet")

    xp_required = models.BigIntegerField(
        help_text="XP required to achieve this level")
    xp_given = models.BigIntegerField(
        help_text="XP allocated to a spirit that defeats a spirit of this "
                  "level"
    )

    cash_given = models.PositiveIntegerField(
        help_text="Cash given when this spirit is defeated")

    item_drop_common = models.ForeignKey(
        "items.Item", related_name="common_drops", blank=True, null=True)
    item_drop_rare = models.ForeignKey(
        "items.Item", related_name="rare_drops", blank=True, null=True)

    def __str__(self):
        return "Lv. {} {}".format(self.level, self.family)
