from django import forms
from django.contrib.gis.db.models import F

from .models import Spirit


class PatchForm(forms.Form):

    ACTIVITY_CHOICES = (
        (Spirit.ACTIVITY_FROLIC, "Frolic"),
        (Spirit.ACTIVITY_JARRED, "Jar"),
        (Spirit.ACTIVITY_WANDER, "Release")
    )

    name = forms.CharField(max_length=255, required=False)
    activity = forms.TypedChoiceField(
        choices=ACTIVITY_CHOICES, required=False, coerce=int)

    def __init__(self, request, spirit, *args, **kwargs):
        forms.Form.__init__(self, *args, **kwargs)
        self.request = request
        self.spirit = spirit

    def clean(self):

        activity = self.cleaned_data["activity"]

        if not self.request.user == self.spirit.owner:
            raise forms.ValidationError(
                "You cannot modify a spirit you do not own"
            )

        if activity == Spirit.ACTIVITY_JARRED:

            if not self.request.user.tree_is_established:
                raise forms.ValidationError(
                    "Your tree is not yet fully rooted.")

            distance_to_tree = self.request.location.distance(
                self.request.user.tree_location
            )
            if distance_to_tree > 0.001:
                raise forms.ValidationError(
                    "You are not close enough to your tree to jar any spirits")

        return self.cleaned_data

    def save(self):

        name = self.cleaned_data["name"]
        activity = self.cleaned_data["activity"]

        kwargs = {}

        if name:
            kwargs["name"] = name

        if activity:
            kwargs["activity"] = activity
            if activity == Spirit.ACTIVITY_WANDER:
                kwargs["owner"] = None
                kwargs["health_current"] = F("health_maximum")
                kwargs["location"] = self.request.location

        if kwargs:
            Spirit.objects.filter(pk=self.spirit.pk).update(**kwargs)
