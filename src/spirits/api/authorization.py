from django.db.models import Q
from tastypie.authorization import Authorization

class SpiritAuthorization(Authorization):

    def read_list(self, object_list, bundle):

        # The finder is a special case of having a GET actually perform a write,
        # so the authentication logic lives inside the SpiritResource
        if bundle.request.GET.get("finder"):
            return object_list

        # Authenticated users can see their spirits and public spirits
        if bundle.request.user.is_authenticated():
            return object_list.filter(
                Q(owner=bundle.request.user) |
                Q(owner__is_public=True)
            )

        # Everyone else can only see public spirits
        return object_list.filter(owner__is_public=True)


    def read_detail(self, object_list, bundle):

        # Wandering spirits are publicly viewable
        if not bundle.obj.owner:
            return True

        # Spirits belonging to users who are tagged as public are by extension
        # public themselves
        if bundle.obj.owner.is_public:
            return True

        # Spirits owned by private users are still visible to their owners
        if bundle.request.user.is_authenticated():
            if bundle.obj.owner == bundle.request.user:
                return True

        return False


    def create_list(self, object_list, bundle):
        return []


    def create_detail(self, object_list, bundle):
        return False


    def update_list(self, object_list, bundle):
        return []


    def update_detail(self, object_list, bundle):
        if object_list:
            if bundle.request.user == object_list[0].owner:
                return True
        return False


    def delete_list(self, object_list, bundle):
        return []


    def delete_detail(self, object_list, bundle):
        return False
