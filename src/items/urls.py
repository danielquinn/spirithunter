from __future__ import absolute_import

from django.conf.urls import url
from .views import ShopView as Shop

urlpatterns = [
    url(r"^shoppe/$", Shop.as_view(), name="items-shop"),
]
