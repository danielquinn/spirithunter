var Listing = function(){

  this.draw_listing = function(){

    var $target = $("#items").html("");

    var url = window.spirithunter.urls.api.v1.items.listing;
    $.getJSON(url, function(data){

      for (var i = 0; i < data.objects.length; i++) {

        var item = data.objects[i];

        $target.append(
          '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 media">' +
            '<a class="pull-left" href="#"><img class="media-object" src="' + item.images["64"] + '" width="64" height="64" /></a>' +
            '<div class="media-body"><h4 class="media-heading">' + item.name + '</h4></div>' +
            '<div class="price"><a href="#" class="btn btn-primary pull-right" style="width: 120px;">Buy ' + item.price + 'cr</a></div>' +
            '<div class="description">' + item.description + '</div>' +
          '</div>'
        );

      }
    });
  };
  return this;
};
