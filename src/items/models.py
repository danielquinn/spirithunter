from datetime import timedelta
from django.conf import settings
from django.db import models
from django.db.models import F

from django_extensions.db.fields import AutoSlugField
from polymorphic.models import PolymorphicModel

from aspects.models.elements import Element


class Item(PolymorphicModel):

    STATUS_AVAILABLE = 1
    STATUS_DISCONTINUED = 2
    STATUSES = (
        (STATUS_AVAILABLE, "Available"),
        (STATUS_DISCONTINUED, "Discontinued"),
    )

    name = models.CharField(max_length=128)
    slug = AutoSlugField(populate_from="name")
    description = models.TextField(blank=True, null=True)
    price = models.PositiveIntegerField()
    status = models.PositiveIntegerField(choices=STATUSES)

    def __str__(self):
        return self.name

    def give_to_user(self, user, quantity=1):
        updated = Inventory.objects.filter(item=self, user=user).update(
            quantity=F("quantity") + quantity
        )
        if not updated:
            Inventory.objects.create(item=self, user=user, quantity=quantity)


class ReagentItem(Item):

    class Meta:
        verbose_name = "Reagent"
        verbose_name_plural = "Reagents"


class SpiritItem(Item):
    """
    Items only applicable to spirits.  These enhance spirit stats.
    """

    class Meta:
        verbose_name = "Sundry"
        verbose_name_plural = "Sundries"

    TYPE_CONSUMABLE = 1  # Sticks, seeds
    TYPE_WEAPON = 2  # Swords, tongues, claws
    TYPE_ARMOUR = 3  # Breastplates, cloaks
    TYPE_ACCESSORY = 4  # Bracelets, gloves, shoes, bottles, belts
    TYPE_REAGENT = 5  # Combine these to make other items
    TYPES = (
        (TYPE_CONSUMABLE, "Consumable"),
        (TYPE_WEAPON,     "Weapon"),
        (TYPE_ARMOUR,     "Armour"),
        (TYPE_ACCESSORY,  "Accessory"),
        (TYPE_REAGENT,    "Reagent"),
    )

    DURATION_PERMANENT = 1  # One-time buffs like HP+20
    DURATION_WHILE_APPLIED = 2  # Weapons, armour, accessories, bottles
    DURATION_TEMPORARY_SHORT = 3  # Consumables
    DURATION_TEMPORARY_MEDIUM = 4
    DURATION_TEMPORARY_LONG = 5
    DURATIONS = (
        (DURATION_PERMANENT,        "Permanent"),
        (DURATION_WHILE_APPLIED,    "As long as it's equipped"),
        (DURATION_TEMPORARY_SHORT,  "15 minutes"),
        (DURATION_TEMPORARY_MEDIUM, "3 hours"),
        (DURATION_TEMPORARY_LONG,   "24 hours"),
    )

    type = models.PositiveIntegerField(choices=TYPES)
    duration = models.PositiveIntegerField(choices=DURATIONS)
    health = models.IntegerField(default=0)
    attack = models.IntegerField(default=0)
    defence = models.IntegerField(default=0)
    element = models.IntegerField(
        choices=Element.ELEMENTS, blank=True, null=True)

    def get_duration(self):
        if self.effect == self.EFFECT_TEMPORARY_SHORT:
            return timedelta(minutes=15)
        if self.effect == self.EFFECT_TEMPORARY_MEDIUM:
            return timedelta(hours=3)
        if self.effect == self.EFFECT_TEMPORARY_LONG:
            return timedelta(days=1)


class BeltUserItem(Item):
    """
    Belts affect a user's capacity to carry spirits.
    """

    class Meta:
        verbose_name = "Belt"
        verbose_name_plural = "Belts"

    capacity = models.IntegerField(default=0)


class BottleUserItem(Item):
    """
    Bottles allow a user to carry a spirit (of a particular level) around.
    """

    class Meta:
        verbose_name = "Bottle"
        verbose_name_plural = "Bottles"

    max_level = models.IntegerField(default=0)


class Recipe(models.Model):

    result = models.ForeignKey(Item, related_name="derived_from")

    def __str__(self):

        name = self.result.name
        article = "a"
        if name.lower().startswith(("a", "e", "i", "o", "u")):
            article = "an"

        return "Recipe for {article} {name}".format(
            article=article,
            name=name
        )


class Ingredient(models.Model):
    recipe = models.ForeignKey(Recipe, related_name="ingredients")
    item = models.ForeignKey(Item)
    quantity = models.PositiveIntegerField()


class Inventory(models.Model):
    """
    Assigns a quantity of items to a user.  If an item in here and it's not a
    bottle or a belt, it's considered to be unused.
    """

    class Meta:
        verbose_name = "Inventory Item"
        verbose_name_plural = "Inventory Items"
        unique_together = ("user", "item")

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="inventory_items")
    item = models.ForeignKey(Item, related_name="users_holding")
    quantity = models.PositiveIntegerField()

    def __str__(self):
        return "{name} (x{quantity:d})".format(
            name=self.item.name,
            quantity=self.quantity
        )


class Application(models.Model):
    """
    Tracks which item is applied/equipped to which spirit when and for how
    long.
    """

    class Meta:
        verbose_name = "Item application"
        verbose_name_plural = "Item applications"

    item = models.ForeignKey(Item, related_name="applications")
    spirit = models.ForeignKey("spirits.Spirit", related_name="applications")
    applied_at = models.DateTimeField(auto_now_add=True)
    expires_at = models.DateTimeField(blank=True, null=True)

    def save(self, *args, **kwargs):

        if not self.expires_at and self.item.type == Item.TYPE_CONSUMABLE:
            self.expires_at = self.applied_at + self.item.get_duration()

        models.Model.save(self, *args, **kwargs)
