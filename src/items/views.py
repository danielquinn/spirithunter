from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView

from rest_framework import viewsets

from .models import Item, Inventory, Recipe, Ingredient
from .serializers import (
    ItemSerializer,
    InventorySerializer,
    RecipeSerializer,
    IngredientSerializer
)


class ItemViewSet(viewsets.ModelViewSet):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer


class IngredientViewSet(viewsets.ModelViewSet):
    queryset = Ingredient.objects.all()
    serializer_class = IngredientSerializer


class RecipeViewSet(viewsets.ModelViewSet):
    queryset = Recipe.objects.all()
    serializer_class = RecipeSerializer


class InventoryViewSet(viewsets.ModelViewSet):
    queryset = Inventory.objects.all()
    serializer_class = InventorySerializer


class ShopView(LoginRequiredMixin, TemplateView):
    """
    List items for sale
    """

    template_name = "items/store.html"
