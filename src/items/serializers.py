from rest_framework import serializers
from .models import Item, Inventory, Recipe, Ingredient


class ItemSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Item
        fields = ("name", "slug", "description", "price", "status")


class InventorySerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Inventory
        fields = ("user", "item", "quantity")


class RecipeSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Recipe
        fields = ("result", "ingredients")


class IngredientSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Ingredient
        fields = ("recipe", "item", "quantity")
