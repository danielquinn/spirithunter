from django.contrib import admin

from .models import (
    ReagentItem, SpiritItem, BeltUserItem, BottleUserItem, Recipe, Ingredient)


class ItemAdmin(admin.ModelAdmin):
    list_display = ["name", "description", "price", "status"]


class ReagentItemAdmin(ItemAdmin):
    pass


class SpiritItemAdmin(ItemAdmin):
    list_display = ItemAdmin.list_display + [
        "type", "duration", "health", "attack", "defence", "element"]


class BeltUserItemAdmin(ItemAdmin):
    list_display = ItemAdmin.list_display + ["capacity"]


class BottleUserItemAdmin(ItemAdmin):
    list_display = ItemAdmin.list_display + ["max_level"]


class IngredientInline(admin.TabularInline):
    model = Ingredient
    extra = 0


class RecipeAdmin(admin.ModelAdmin):

    list_display = ("result", "get_ingredients")
    inlines = (IngredientInline,)

    def get_ingredients(self, obj):
        return ", ".join(obj.ingredients.values_list("item__name", flat=True))

    get_ingredients.short_description = "Ingredients"


admin.site.register(ReagentItem, ReagentItemAdmin)
admin.site.register(SpiritItem, SpiritItemAdmin)
admin.site.register(BeltUserItem, BeltUserItemAdmin)
admin.site.register(BottleUserItem, BottleUserItemAdmin)
admin.site.register(Recipe, RecipeAdmin)
