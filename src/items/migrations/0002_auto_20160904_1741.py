# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-04 17:41
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('spirits', '0001_initial'),
        ('items', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='inventory',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='inventory_items', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='ingredient',
            name='item',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='items.Item'),
        ),
        migrations.AddField(
            model_name='ingredient',
            name='recipe',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ingredients', to='items.Recipe'),
        ),
        migrations.AddField(
            model_name='application',
            name='item',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='applications', to='items.Item'),
        ),
        migrations.AddField(
            model_name='application',
            name='spirit',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='applications', to='spirits.Spirit'),
        ),
        migrations.AlterUniqueTogether(
            name='inventory',
            unique_together=set([('user', 'item')]),
        ),
    ]
