from django.db.models.fields.related import ForeignKey, OneToOneField
from django.templatetags.static import static
from tastypie.resources import ModelResource
from tastypie import fields

from .models import Item, Inventory

class ItemResource(ModelResource):

    class Meta:
        allowed_methods = ("get",)
        queryset = Item.objects.all()
        resource_name = "items"

    def dehydrate(self, bundle):
        """
        Account for django-polymorphic
        """

        unacceptable_field_types = (ForeignKey, OneToOneField)

        for field in bundle.obj._meta.fields:
            if field.name in bundle.data:
                continue
            if not isinstance(field, unacceptable_field_types):
                bundle.data[field.name] = getattr(bundle.obj, field.name)

        bundle.data["images"] = {}
        for size in (32, 64, 128):
            bundle.data["images"][str(size)] = static(
                "items/img/{size}/{pk}.png".format(
                    size=size,
                    pk=bundle.obj.pk
                )
            )

        return bundle.data



class InventoryResource(ModelResource):

    user = fields.ToOneField("users.api.UserResource", "user")
    item = fields.ToOneField(ItemResource, "item")

    class Meta:
        allowed_methods = ("get",)
        queryset = Inventory.objects.all()
        resource_name = "inventory"
        filtering = {
            "user": ("exact",)
        }
