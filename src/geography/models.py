from django.contrib.gis.db import models
from django_countries.fields import CountryField


class ExclusiveEconomicZone(models.Model):
    """
    This dataset is compiled by hand by merging two datasets:

    * The MarineRegions.org EEZ .shp file
        * Lists countries by ISO3
        * http://www.marineregions.org/sources.php#unioneezcountry
    * The GeoNames country listing
        * Lists countries by ISO2 *and* ISO3
        * http://download.geonames.org/export/dump/countryInfo.txt
    """
    geometry = models.MultiPolygonField(srid=4326)
    objects = models.GeoManager()


class Country(models.Model):

    TOURISM_CHANCE = 100  # 1 in n chance of a spirit being a tourist

    TOURISM_HIGH = "high"
    TOURISM_MEDIUM = "medium"
    TOURISM_LOW = "low"
    TOURISM = (
        (TOURISM_HIGH, "High"),
        (TOURISM_MEDIUM, "Medium"),
        (TOURISM_LOW, "LOW")
    )

    country = CountryField(primary_key=True)
    tourism = models.CharField(choices=TOURISM, max_length=6)
    exclusive_economic_zone = models.OneToOneField(
        ExclusiveEconomicZone,
        related_name="country",
        blank=True,
        null=True
    )

    objects = models.GeoManager()

    class Meta:
        verbose_name_plural = "Countries"

    def __str__(self):
        return str(self.country.name)
