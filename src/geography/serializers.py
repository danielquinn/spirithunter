from rest_framework import serializers
from .models import Country


class CountrySerializer(serializers.Serializer):

    code = serializers.CharField(source="country.code")
    name = serializers.CharField(source="country.name")
    flag = serializers.CharField(source="country.flag")

    class Meta:
        model = Country
        fields = ("code", "name", "flag")

