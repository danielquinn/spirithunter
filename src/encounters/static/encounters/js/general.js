/*

 I hate this whole file.  We need a framework, and when we choose one, this
 whole thing will have to be rewritten to be more API dependent and less about
 the DOM.  jQuery just isn't enough here.

 finder:    Where you select a spirit (if any) to attack from a map
 chooser:   Where you decide which spirit you'll be using
 encounter: The theatre of battle
 spoils:    Where you choose your reward
 defeated:  Where you're told that you suck
*/

var Map = function(){

  var self = this;

  this.attacker = {};
  this.defender = {};

  this.$finder    = $("#finder");
  this.$chooser   = $("#chooser");
  this.$encounter = $("#encounter");
  this.$spoils    = $("#spoils");
  this.$defeated  = $("#defeated");

  this.centre = window.spirithunter.location;
  this.map = null;

  this.$chooser.find("a").click(function(e){
    self.handle_attacker_selection.call(this, e);
  });

  $("#use-attack").click(function(event){
    self.handle_attack($(this));
  });

  $("#escape").click(function(event){
    event.preventDefault();
    self.show_finder();
  });


  this.show_finder = function(){

    this.$chooser.hide();
    this.$encounter.hide();
    this.$finder.show();
    this.$spoils.hide();
    this.$defeated.hide();

    if (this.map) {
      return;
    }

    this.map = L.map('finder').setView(this.centre, 18);
    L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
    }).addTo(this.map);

    var url = window.spirithunter.urls.api.v1.spirits.listing + "?finder=1&activity=" + window.spirithunter.constants.activities.wander;
    $.getJSON(url, function(data){
      for (var i = 0; i < data.objects.length; i++){

        var spirit = data.objects[i];
        var icon = L.icon({
          iconUrl: spirit.images["64"],
          iconRetinaUrl: spirit.images["128"],
          iconSize: [64, 64],
          popupAnchor: [0, -16]
        });

        var aspects = "";
        for (var a = 0; a < spirit.elementals.length; a++) {
          aspects += ' <img src="' + spirit.elementals[a].element.images["16"] + '" title="' + spirit.elementals[a].element.name + '" />';
        }
        for (var b = 0; b < spirit.facets.length; b++) {
          aspects += ' <img src="' + spirit.facets[b].images["16"] + '" title="' + spirit.facets[b].name + '" />';
        }
        for (var c = 0; c < spirit.nationalities.length; c++) {
          aspects += ' <span class="flag ' + spirit.nationalities[c].code.toLowerCase() + '" title="' + spirit.nationalities[c].name + '"></span>';
        }

        // Reassign `url` since we don't need the old value anymore
        url = window.spirithunter.urls.api.v1.encounters.listing;

        // GeoJSON is lng,lat, while Leaflet is lat,lng #killme
        var location = [
          spirit.location.coordinates[1],
          spirit.location.coordinates[0]
        ];
        var marker = L.marker(location, {icon: icon}).addTo(self.map)
          .bindPopup(
            '<div class="spirit text-center">' +
              '<div class="image"><img src="' + spirit.images["128"] + '" width="128" height="128" /></div>' +
              '<div class="name">' + spirit.name + ' (id:' + spirit.id + ')</div>' +
              '<div class="level">Level: ' + spirit.level + '</div>' +
              '<div class="aspects f16">' + aspects + '</div>' +
              '<div class="options"><a href="' + url + '" class="btn btn-danger btn-sm fight-start">Fight</a></div>' +
            '</div>'
          );
        marker.spirit = spirit;

        marker.on("popupopen", function(event){
          $(".spirit a.fight-start").click(function(e){
            e.preventDefault();
            event.target.closePopup();
            self.defender = event.target.spirit;
            self.show_chooser();
          });
        });

      }
    });

  };


  this.show_chooser = function(){
    this.$finder.hide();
    this.$encounter.hide();
    this.$chooser.show();
    this.$spoils.hide();
    this.$defeated.hide();
  };


  this.show_encounter = function(){

    var attacker = self.attacker;
    var defender = self.defender;

    this.$finder.hide();
    this.$chooser.hide();
    this.$encounter.show();
    this.$spoils.hide();
    this.$defeated.hide();

    // Add Google Street View to the background based on the lat/lng of the defender.  This might just be better with some nice art instead.
    this.$encounter.find(".theatre")
      .css("background-image", "url('https://maps.googleapis.com/maps/api/streetview?location=" + defender.location[1] + "," + defender.location[0] + "&size=" + $(window).width() + "x200')");

    this.$encounter.find(".theatre .attacker").html('<img src="' + attacker.images["64"] + '" width="64" height="64" />');
    this.$encounter.find(".theatre .defender").html('<img src="' + defender.images["64"] + '" width="64" height="64" />');
    this.$encounter.find(".stats .attacker .level .level-value").html(attacker.level);
    this.$encounter.find(".stats .defender .level .level-value").html(defender.level);
    this.$encounter.find(".stats .attacker .health-current").html(attacker.health_current);
    this.$encounter.find(".stats .attacker .health-maximum").html(attacker.health_maximum);
    this.$encounter.find(".stats .defender .health-current").html(defender.health_current);
    this.$encounter.find(".stats .defender .health-maximum").html(defender.health_maximum);

    var $elements_attacker = this.$encounter.find(".theatre .attacker-elements").html("");
    var $elements_defender = this.$encounter.find(".theatre .defender-elements").html("");
    var i;

    for (i = 0; i < attacker.elementals.length; i++) {
      $elements_attacker.append('<img src="' + attacker.elementals[i].element.images["16"] + '" width="16" height="16" />');
    }
    for (i = 0; i < defender.elementals.length; i++) {
      $elements_defender.append('<img src="' + defender.elementals[i].element.images["16"] + '" width="16" height="16" />');
    }

  };


  this.show_post_fight_options = function(encounter_id, url){

    var self = this;

    $.getJSON(url, function(spirit){

      self.$finder.hide();
      self.$chooser.hide();
      self.$encounter.hide();
      self.$spoils.show();
      self.$defeated.hide();

      var $keep = $("#spoils-keep");
      var $item = $("#spoils-item");

      $("#spoils-xp").html(spirit.experience_given);
      $("#spoils-loser-name").html(spirit.name);

      // Draw a a pretty box here with a picture of the spirit and some of its
      // stats using the `spirit` object.

      var action_url = window.spirithunter.urls.api.v1.encounters.detail.replace("0000", encounter_id);

      $keep.click(function(){
        $.ajax({
          type: "PUT",
          url: action_url,
          contentType: "application/json",
          data: '{"decision": "' + window.spirithunter.constants.decisions.capture + '"}',
          success: function(){
            window.location.href = window.spirithunter.urls.finder;
          }
        });
      });

      $item.click(function(){
        $.ajax({
          type: "PUT",
          url: action_url,
          contentType: "application/json",
          data: '{"decision": "' + window.spirithunter.constants.decisions.release + '"}',
          success: function(){
            window.location.href = window.spirithunter.urls.finder;
          }
        });
      });

    });

  };


  this.show_defeated = function(){

    console.log("Defeated");

    this.$finder.hide();
    this.$chooser.hide();
    this.$encounter.hide();
    this.$spoils.hide();
    this.$defeated.show();

  };


  this.handle_attacker_selection = function(event){

    event.preventDefault();

    var elements = $(this).attr("data-elements").split(",");
    self.attacker = {
      id: $(this).attr("data-pk"),
      images: {
        "64": $(this).attr("data-image64")
      },
      level: $(this).attr("data-level"),
      health_current: $(this).attr("data-health_current"),
      health_maximum: $(this).attr("data-health_maximum"),
      elementals: []
    };

    for (var i = 0; i < elements.length; i++) {
      self.attacker.elementals.push({element: {images: {"16": elements[i]}}});
    }
    self.show_encounter();

    // Hack to make the spoils easier
    $("#spoils-winner-name").html($(this).attr("data-name"));

  };


  this.handle_attack = function($button){
    $.ajax({
      url : window.spirithunter.urls.api.v1.encounters.listing,
      type: "POST",
      data : JSON.stringify({
        attacker: self.attacker.id,
        defender: self.defender.id,
        action: window.spirithunter.constants.actions.attack
      }),
      dataType: "json",
      contentType: "application/json",
      processData: false,
      statusCode: { // .success() only fires on a 200 code -- lame.
        "201": function(data, textStatus, jqXHR) {
          self.render_fight_result(data);
        }
      }
    });
  };


  this.render_fight_result = function(data){

    var attacker = self.attacker;
    var defender = self.defender;

    self.attacker.health_current = data.user_spirit_health_after;
    self.defender.health_current = data.bot_spirit_health_after;

    // Change the HP values
    self.$encounter.find(".stats .attacker .health-current").html(
      self.attacker.health_current
    );
    self.$encounter.find(".stats .defender .health-current").html(
      self.defender.health_current
    );

    switch (data.result) {
      case window.spirithunter.constants.results.defeated:
        self.show_defeated();
        break;
      case window.spirithunter.constants.results.finished:
        self.show_post_fight_options(data.id, data.bot_spirit);
        break;
    }

  };

  return this;

};


// __main__ -------------------------------------------------------------------

$(document).on("location.acquired", function(){
  var map = new Map();
  map.show_finder();
});
