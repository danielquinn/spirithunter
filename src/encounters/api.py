from random import choice, randint, randrange

from django.db.models.aggregates import Sum
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404

from tastypie.authentication import BasicAuthentication, SessionAuthentication, MultiAuthentication
from tastypie.resources import ModelResource
from tastypie.exceptions import BadRequest
from tastypie import fields, http

from economy.models import SpoilsTransaction
from aspects.models import Element
from spirits.api.resources import SpiritResource
from spirits.models.spirit import Spirit

from .models import Encounter

class EncounterResource(ModelResource):

    user_spirit = fields.ToOneField(SpiritResource, "user_spirit")
    bot_spirit  = fields.ToOneField(SpiritResource, "bot_spirit")

    class Meta:
        allowed_methods = ("get", "post", "put")
        authentication = MultiAuthentication(
            SessionAuthentication(),
            BasicAuthentication()
        )
        queryset = Encounter.objects.all()
        object_class = Encounter
        always_return_data = True
        resource_name = "encounters"


    def obj_create(self, bundle, **kwargs):
        """
        Issue a POST to initiate an encounter with a spirit.
        """

        user_spirit = get_object_or_404(
            Spirit,
            pk=bundle.data.get("attacker"),
            owner=bundle.request.user,
            health_current__gt=0
        )

        bot_spirit = get_object_or_404(
            Spirit,
            pk=bundle.data.get("defender"),
            health_current__gt=0
        )

        if not user_spirit.owner == bundle.request.user:
            raise BadRequest("You can only start a fight using your own spirit")

        if bot_spirit.activity != Spirit.ACTIVITY_WANDER:
            raise BadRequest("Only wandering spirits may be targeted")

        if bot_spirit.health_current <= 0:
            raise BadRequest("This spirit has already been defeated")

        # User action
        user_spirit_action = bundle.data.get("action")
        if not user_spirit_action:
            raise BadRequest("Action is required")
        acceptable_actions = (Encounter.ACTION_ATTACK, Encounter.ACTION_ITEM)
        if user_spirit_action not in acceptable_actions:
            raise BadRequest("Invalid action specified")

        # Set user_spirit_element to None, an Element object, or just 404 out
        user_spirit_element = bundle.data.get("element")
        if user_spirit_element:
            user_spirit_element = get_object_or_404(
                Element,
                pk=user_spirit_element,
                spirits=user_spirit
            )

        # Select bot element (if any)
        bot_spirit_element = choice(list(bot_spirit.elements.all()) + [None])

        user_health, bot_health, result = self._do_violence(
            user_spirit,
            user_spirit_action,
            user_spirit_element,
            bot_spirit,
            bot_spirit_element
        )

        bundle.obj = Encounter.objects.create(
            user=bundle.request.user,
            user_spirit=user_spirit,
            user_spirit_action=user_spirit_action,
            user_spirit_element=user_spirit_element,
            user_spirit_health_before=user_spirit.health_current,
            user_spirit_health_after=user_health,
            bot_spirit=bot_spirit,
            bot_spirit_action=Encounter.ACTION_ATTACK,
            bot_spirit_element=bot_spirit_element,
            bot_spirit_health_before=bot_spirit.health_current,
            bot_spirit_health_after=bot_health,
            location=bot_spirit.location,
            result=result
        )

        return bundle


    def put_list(self, request, **kwargs):
        return http.HttpNotImplemented()


    def obj_update(self, bundle, skip_errors=False, **kwargs):
        """
        Issue a PUT request to state your decision in the event of an encounter
        win.  Either keep the spirit, or release it, giving you the chance to
        receive a reward.

        NOTE: This method is sort of a hack on top of tastypie's usual behaviour
        NOTE: as PUT is considered a full overwrite and PATCH a partial update,
        NOTE: and we're effectively opting for neither in this case.  However
        NOTE: tastypie's default behaviour for PUT requests is to set bundle.obj
        NOTE: to None, so we're just working around that to make things work.
        """

        # Hack (see above)
        bundle.obj = Encounter.objects.get(pk=kwargs.get("pk"))

        if not bundle.obj.result == Encounter.RESULT_FINISHED:
            raise BadRequest("Encounters cannot be resolved until one party wins")

        user = bundle.request.user

        available_decisions = (
            str(Encounter.DECISION_CAPTURE),
            str(Encounter.DECISION_RELEASE)
        )
        decision = bundle.data.get("decision")
        if decision not in available_decisions:
            raise BadRequest(
                "Invalid decision value. Must be one of %s" % ", ".join(
                    available_decisions
                )
            )

        if not user == bundle.obj.user:
            raise BadRequest("You did not take part in that encounter")

        # Allocate experience
        user_spirit = bundle.obj.user_spirit
        bot_spirit = bundle.obj.bot_spirit
        bot_level = bot_spirit.family.ladder.get(level=bot_spirit.level)
        user_spirit.give_xp(bot_level.xp_given)

        if decision == str(Encounter.DECISION_CAPTURE):
            # Change ownership
            Spirit.objects.filter(pk=bot_spirit.pk).update(
                owner=user,
                activity=Spirit.ACTIVITY_FROLIC
            )
            bundle.data["spirit"] = reverse("api_dispatch_detail", kwargs={
                "api_name": "v1",
                "resource_name": "spirits",
                "pk": bot_spirit.pk
            })
            return bundle

        # Cash
        SpoilsTransaction.objects.create(
            user=user,
            delta=bot_level.cash_given,
            encounter=bundle.obj,
        )
        bundle.data["coins"] = bot_level.cash_given

        # Item
        if bot_level.item_drop_rare and not randint(0, 8):
            bot_level.item_drop_rare.give_to_user(user)
            bundle.data["item"] = reverse("api_dispatch_detail", kwargs={
                "api_name": "v1",
                "resource_name": "items",
                "pk": bot_level.item_drop_rare_id
            })
        elif bot_level.item_drop_common and not randint(0, 2):
            bot_level.item_drop_common.give_to_user(user)
            bundle.data["item"] = reverse("api_dispatch_detail", kwargs={
                "api_name": "v1",
                "resource_name": "items",
                "pk": bot_level.item_drop_common_id
            })

        # Release the spirit
        Spirit.objects.filter(pk=bot_spirit.pk).update(
            activity=Spirit.ACTIVITY_KILLED
        )

        return bundle


    @staticmethod
    def dehydrate_location(bundle):
        return bundle.obj.location.geojson


    def _do_violence(self, user_spirit, user_spirit_action, user_spirit_element,
                     bot_spirit, bot_spirit_element):
        """
        Pick a fight with this spirit using a spirit of my own.
        """

        # Fastest spirit goes first
        user_speed = float(user_spirit.speed) * float(randrange(start=0, stop=2000)) / 1000
        bot_speed  = float(bot_spirit.speed)  * float(randrange(start=0, stop=2000)) / 1000

        if bot_speed > user_speed:
            user_health, bot_health = self._fight(
                bot_spirit,
                Encounter.ACTION_ATTACK,
                bot_spirit_element,
                user_spirit,
                user_spirit_action,
                user_spirit_element
            )
        else:
            bot_health, user_health = self._fight(
                user_spirit,
                user_spirit_action,
                user_spirit_element,
                bot_spirit,
                Encounter.ACTION_ATTACK,
                bot_spirit_element
            )

        result = Encounter.RESULT_NOTHING
        if bot_health == 0:
            result = Encounter.RESULT_FINISHED
        elif user_health == 0:
            result = Encounter.RESULT_DEFEATED

        return user_health, bot_health, result


    def _fight(self, first_spirit, first_action, first_element,
               second_spirit, second_action, second_element):

        r = [
            self._engage(
                first_spirit,
                second_spirit,
                action=first_action,
                element=first_element
            ),
            first_spirit.health_current
        ]
        if r[0] > 0:  # r[0] is the defender's health remaining from that fight
            r[1] = self._engage(
                second_spirit,
                first_spirit,
                action=second_action,
                element=second_element
            )

        return r


    @staticmethod
    def _engage(attacker, defender, action, element=None):
        """
        A spirit has a day aspect of +2 and a water aspect of +1 and an Attack
        of 5.  A non elemental attack does 3 day damage and 2 water damage.  An
        elemental attack of day does 5 day damage.

        The spirit defending has a defence of 2 and a day aspect of -2 and a
        water aspect of +1.
        From the non elemental attack it feels the effects of the attack as if
        it was 3+2 day damage and 2-1 water damage for a total of 6 damage which
        is then reduced by its defence to 4 damage.
        From the elemental attack it feels the effects of the attack as if it
        was 5+2 day damage for a total of 7 which the defence then reduces to 5.

        In a situation where the environment has a +1 day aspect the poor
        defending spirit suffers an additional point of day damage on any day
        attack.

        If the spirit defending has a defence of 2 and a day aspect of +2.
        From the non elemental attack it feels the effect of the attack as if it
        was 3-2 day damage and 2 water damage for a total of 3 which its defence
        then lowers to 1.
        From the elemental attack it feels the effects of the attack as if it
        was 5-2 day damage for a total of 3 which its defence then lowers to 1.
        """

        if action == Encounter.ACTION_ITEM:
            raise NotImplementedError("Item stuffs not ready yet")

        attack  = attacker.attack * float(randint(80, 200)) / 100
        defence = defender.defence * float(randint(50, 120)) / 100

        # Elemental
        if element:

            damage = attack

            defender_element_defence = defender.elemental_strengths.filter(
                element=element
            ).aggregate(
                Sum("strength")
            ).values()[0]
            if defender_element_defence:
                damage -= defence

        # Non-elemental
        else:

            # Compile all positive attributes for the attacker
            attacker_attributes = attacker.elemental_strengths.filter(strength__gte=0)
            attacker_attributes = dict([
                (a.element, a.strength) for a in attacker_attributes
            ])
            attributes_total = float(sum(attacker_attributes.values()))

            # Do the same for the defender
            defender_attributes = defender.elemental_strengths.all()
            defender_attributes = dict([
                (d.element, d.strength) for d in defender_attributes
            ])

            # Build a dictionary with element:attack values
            potential_damages = {}
            for element, attribute in attacker_attributes.items():
                potential_damages[element] = round(attack * (
                    float(attribute) / attributes_total
                ))

            # Offset the damages with the defence of the defender
            actual_damages = {}
            for element, attribute in attacker_attributes.items():
                actual_damages[element] = potential_damages[element]
                if element in defender_attributes:
                    damage_attempted = potential_damages[element]
                    damage_avoided = defender_attributes[element]
                    actual_damages[element] = damage_attempted - damage_avoided

            damage = sum(actual_damages.values())

        damage = max(1, int(damage))

        health_before = defender.health_current
        if defender.health_current < damage:
            Spirit.objects.filter(pk=defender.pk).update(health_current=0)
        else:
            Spirit.objects.filter(pk=defender.pk).update(
                health_current=(health_before - damage)
            )  # TODO: Possible race condition

        # Hit the db again in case of a race condition
        health_after = Spirit.objects.get(pk=defender.pk).health_current

        return health_after
