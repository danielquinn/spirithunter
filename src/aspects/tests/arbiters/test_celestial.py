import ephem

from django.test import TestCase

from aspects.arbiters.celestial import CelestialArbiter


class CelestialTest(TestCase):

    VANCOUVER = (49, -123)
    SEOUL = (37, 53)
    ARBITRARY_DATE = ephem.Date((2010, 1, 1, 0, 0, 0))

    vancouver = CelestialArbiter(*VANCOUVER)
    seoul = CelestialArbiter(*SEOUL)

    def setUp(self):
        self.vancouver.observer.date = self.ARBITRARY_DATE
        self.seoul.observer.date = self.ARBITRARY_DATE

    def test_sun_up(self):
        self.assertTrue(self.vancouver._sun_up())
        self._test(self.vancouver, "_neptune_up", True)

    def test_sun_down(self):
        self.assertFalse(self.seoul._sun_up())
        self._test(self.vancouver, "_neptune_up", True)

    def test_mercury_up(self):
        self.assertTrue(self.vancouver._mercury_up())
        self._test(self.vancouver, "_neptune_up", True)

    def test_mercury_down(self):
        self.assertFalse(self.seoul._mercury_up())
        self._test(self.vancouver, "_neptune_up", True)

    def test_venus_up(self):
        self.assertTrue(self.vancouver._venus_up())
        self._test(self.vancouver, "_neptune_up", True)

    def test_venus_down(self):
        self.assertFalse(self.seoul._venus_up())
        self._test(self.vancouver, "_neptune_up", True)

    def test_mars_up(self):
        self.assertTrue(self.seoul._mars_up())
        self._test(self.vancouver, "_neptune_up", True)

    def test_mars_down(self):
        self.assertFalse(self.vancouver._mars_up())
        self._test(self.vancouver, "_neptune_up", True)

    def test_jupiter_up(self):
        self.assertTrue(self.vancouver._jupiter_up())
        self._test(self.vancouver, "_neptune_up", True)

    def test_jupiter_down(self):
        self._test(self.seoul, "_jupiter_up", False)

    def test_saturn_up(self):
        self._test(self.seoul, "_saturn_up", True)

    def test_saturn_down(self):
        self._test(self.vancouver, "_saturn_up", False)

    def test_uranus_up(self):
        self._test(self.vancouver, "_uranus_up", True)

    def test_uranus_down(self):
        self._test(self.seoul, "_uranus_up", False)

    def test_neptune_up(self):
        self._test(self.vancouver, "_neptune_up", True)

    def test_neptune_down(self):
        self._test(self.seoul, "_neptune_up", False)

    # def test_moon_full(self):
    #     self.assertTrue(self.vancouver._moon_full)

    def _test(self, arbiter, method, polarity):
        arbiter.observer.date = self.ARBITRARY_DATE
        getattr(self, "assert{}".format(polarity))(getattr(arbiter, method)())
