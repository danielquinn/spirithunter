import arrow

from unittest import mock

from pytz import timezone
from django.test import TestCase
from django.contrib.gis.geos import fromstr

from citytemp.models import City
from aspects.arbiters.time import TimeArbiter

VANCOUVER = (49, -123)
SEOUL = (37, 53)


def fake_national_day_ca(tz):
    return arrow.get(2014, 7, 1, 23).astimezone(timezone(tz))


def fake_national_day_gr(tz):
    return arrow.get(2014, 3, 25, 23).astimezone(timezone(tz))


def fake_national_day_nl(tz):
    return arrow.get(2014, 4, 26, 23).astimezone(timezone(tz))


def fake_national_day_us(tz):
    return arrow.get(2014, 7, 4, 23).astimezone(timezone(tz))


def fake_pi_day(tz):
    return arrow.get(2014, 3, 14, 23).astimezone(timezone(tz))


def fake_starwars_day(tz):
    return arrow.get(2014, 5, 4, 23).astimezone(timezone(tz))


class TimeTest(TestCase):

    def setUp(self):
        City.objects.create(
            country="CA",
            city="Vancouver",
            airport_code="YVR",
            location=fromstr("POINT({lng} {lat})".format(
                lat=VANCOUVER[0], lng=VANCOUVER[1])),
            elevation=10,
            timezone="Canada/Pacific"
        )
        City.objects.create(
            country="KR",
            city="Seoul",
            airport_code="GMP",
            location=fromstr("POINT({lng} {lat})".format(
                lat=SEOUL[0], lng=SEOUL[1])),
            elevation=10,
            timezone="Asia/Seoul"
        )

    @mock.patch("arrow.now", fake_national_day_ca)
    def test_time_national_day_ca(self):

        vancouver = TimeArbiter(VANCOUVER[0], VANCOUVER[1])
        self.assertTrue(vancouver._is_national_day_ca())
        self.assertFalse(vancouver._is_national_day_gr())
        self.assertFalse(vancouver._is_national_day_nl())
        self.assertFalse(vancouver._is_national_day_us())
        self.assertFalse(vancouver._is_pi_day())
        self.assertFalse(vancouver._is_starwars_day())

        seoul = TimeArbiter(SEOUL[0], SEOUL[1])
        self.assertFalse(seoul._is_national_day_ca())
        self.assertFalse(seoul._is_national_day_gr())
        self.assertFalse(seoul._is_national_day_nl())
        self.assertFalse(seoul._is_national_day_us())
        self.assertFalse(seoul._is_pi_day())
        self.assertFalse(seoul._is_starwars_day())

    @mock.patch("arrow.now", fake_national_day_gr)
    def test_time_national_day_gr(self):

        vancouver = TimeArbiter(VANCOUVER[0], VANCOUVER[1])
        self.assertFalse(vancouver._is_national_day_ca())
        self.assertTrue(vancouver._is_national_day_gr())
        self.assertFalse(vancouver._is_national_day_nl())
        self.assertFalse(vancouver._is_national_day_us())
        self.assertFalse(vancouver._is_pi_day())
        self.assertFalse(vancouver._is_starwars_day())

        seoul = TimeArbiter(SEOUL[0], SEOUL[1])
        self.assertFalse(seoul._is_national_day_ca())
        self.assertFalse(seoul._is_national_day_gr())
        self.assertFalse(seoul._is_national_day_nl())
        self.assertFalse(seoul._is_national_day_us())
        self.assertFalse(seoul._is_pi_day())
        self.assertFalse(seoul._is_starwars_day())

    @mock.patch("arrow.now", fake_national_day_nl)
    def test_time_national_day_nl(self):

        vancouver = TimeArbiter(VANCOUVER[0], VANCOUVER[1])
        self.assertFalse(vancouver._is_national_day_ca())
        self.assertFalse(vancouver._is_national_day_gr())
        self.assertTrue(vancouver._is_national_day_nl())
        self.assertFalse(vancouver._is_national_day_us())
        self.assertFalse(vancouver._is_pi_day())
        self.assertFalse(vancouver._is_starwars_day())

        seoul = TimeArbiter(SEOUL[0], SEOUL[1])
        self.assertFalse(seoul._is_national_day_ca())
        self.assertFalse(seoul._is_national_day_gr())
        self.assertFalse(seoul._is_national_day_nl())
        self.assertFalse(seoul._is_national_day_us())
        self.assertFalse(seoul._is_pi_day())
        self.assertFalse(seoul._is_starwars_day())

    @mock.patch("arrow.now", fake_national_day_us)
    def test_time_national_day_us(self):

        vancouver = TimeArbiter(VANCOUVER[0], VANCOUVER[1])
        self.assertFalse(vancouver._is_national_day_ca())
        self.assertFalse(vancouver._is_national_day_gr())
        self.assertFalse(vancouver._is_national_day_nl())
        self.assertTrue(vancouver._is_national_day_us())
        self.assertFalse(vancouver._is_pi_day())
        self.assertFalse(vancouver._is_starwars_day())

        seoul = TimeArbiter(SEOUL[0], SEOUL[1])
        self.assertFalse(seoul._is_national_day_ca())
        self.assertFalse(seoul._is_national_day_gr())
        self.assertFalse(seoul._is_national_day_nl())
        self.assertFalse(seoul._is_national_day_us())
        self.assertFalse(seoul._is_pi_day())
        self.assertFalse(seoul._is_starwars_day())

    @mock.patch("arrow.now", fake_pi_day)
    def test_time_pi_day(self):

        vancouver = TimeArbiter(VANCOUVER[0], VANCOUVER[1])
        self.assertFalse(vancouver._is_national_day_ca())
        self.assertFalse(vancouver._is_national_day_gr())
        self.assertFalse(vancouver._is_national_day_nl())
        self.assertFalse(vancouver._is_national_day_us())
        self.assertTrue(vancouver._is_pi_day())
        self.assertFalse(vancouver._is_starwars_day())

        seoul = TimeArbiter(SEOUL[0], SEOUL[1])
        self.assertFalse(seoul._is_national_day_ca())
        self.assertFalse(seoul._is_national_day_gr())
        self.assertFalse(seoul._is_national_day_nl())
        self.assertFalse(seoul._is_national_day_us())
        self.assertFalse(seoul._is_pi_day())
        self.assertFalse(seoul._is_starwars_day())

    @mock.patch("arrow.now", fake_starwars_day)
    def test_time_starwars_day(self):

        vancouver = TimeArbiter(VANCOUVER[0], VANCOUVER[1])
        self.assertFalse(vancouver._is_national_day_ca())
        self.assertFalse(vancouver._is_national_day_gr())
        self.assertFalse(vancouver._is_national_day_nl())
        self.assertFalse(vancouver._is_national_day_us())
        self.assertFalse(vancouver._is_pi_day())
        self.assertTrue(vancouver._is_starwars_day())

        seoul = TimeArbiter(SEOUL[0], SEOUL[1])
        self.assertFalse(seoul._is_national_day_ca())
        self.assertFalse(seoul._is_national_day_gr())
        self.assertFalse(seoul._is_national_day_nl())
        self.assertFalse(seoul._is_national_day_us())
        self.assertFalse(seoul._is_pi_day())
        self.assertFalse(seoul._is_starwars_day())
