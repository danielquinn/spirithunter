from django.contrib import admin

from .models import Element, Facet, WeatherCounter


class ElementAdmin(admin.ModelAdmin):

    list_display = ("name",)

    @staticmethod
    def name(obj):
        return str(obj)


class FacetAdmin(admin.ModelAdmin):
    list_display = ("name",)


class WeatherCounterAdmin(admin.ModelAdmin):
    list_display = ("time", "limit_type", "call_total")


admin.site.register(Element, ElementAdmin)
admin.site.register(Facet, FacetAdmin)
admin.site.register(WeatherCounter, WeatherCounterAdmin)
