import arrow

from citytemp.models import City
from django.contrib.gis.geos import *
from django.contrib.gis.measure import D

from ..models import Facet
from .base import Arbiter


class TimeArbiter(Arbiter):

    WEIGHT_NATIONAL_DAY = 2
    WEIGHT_PI = 10
    WEIGHT_TOWEL = 10
    WEIGHT_STARWARS = 10

    def __init__(self, lat, lng):
        """
        Get an updated copy of city-to-timezone mapping here:
        http://www.citytimezones.info/pending_requests.htm or here:
        http://efele.net/maps/tz/world/
        """

        centre = fromstr("POINT({lng} {lat})".format(lat=lat, lng=lng))
        self.now = arrow.now(
            City.objects.filter(
                location__distance_lte=(centre, D(km=1000))
            ).distance(
                centre
            ).order_by(
                "distance"
            ).first().timezone
        )

    def get_results(self):

        r = {"elements": [], "facets": [], "nationalities": []}

        if self._is_pi_day():
            r["facets"].append((Facet.FACET_PI, self.WEIGHT_PI))
        elif self._is_starwars_day():
            r["facets"].append(
                (Facet.FACET_STARWARS, self.WEIGHT_STARWARS))
        elif self._is_national_day_ca():
            r["nationalities"].append(("CA", self.WEIGHT_NATIONAL_DAY))
        elif self._is_national_day_gr():
            r["nationalities"].append(("GR", self.WEIGHT_NATIONAL_DAY))
        elif self._is_national_day_nl():
            r["nationalities"].append(("NL", self.WEIGHT_NATIONAL_DAY))
        elif self._is_national_day_us():
            r["nationalities"].append(("US", self.WEIGHT_NATIONAL_DAY))

        return r

    def _is_pi_day(self):
        return self.now.month == 3 and self.now.day == 14

    def _is_starwars_day(self):
        return self.now.month == 5 and self.now.day == 4

    def _is_national_day_ca(self):
        return self.now.month == 7 and self.now.day == 1

    def _is_national_day_gr(self):
        return self.now.month == 3 and self.now.day == 25

    def _is_national_day_nl(self):
        if self.now.year == 2014:
            return self.now.month == 4 and self.now.day == 26
        return self.now.month == 4 and self.now.day == 27

    def _is_national_day_us(self):
        return self.now.month == 7 and self.now.day == 4
