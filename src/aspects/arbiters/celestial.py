import ephem

from .base import Arbiter
from ..models import Facet, Element


class CelestialArbiter(Arbiter):

    WEIGHT_DAY = 5
    WEIGHT_NIGHT = 5
    WEIGHT_MOON_FULL = 5
    WEIGHT_MOON_NEW = 5
    WEIGHT_MERCURY = 1
    WEIGHT_VENUS = 1
    WEIGHT_MARS = 1
    WEIGHT_JUPITER = 1
    WEIGHT_SATURN = 1
    WEIGHT_URANUS = 1
    WEIGHT_NEPTUNE = 1

    def __init__(self, lat, lng):

        self.observer = ephem.Observer()

        # Using str() is necessary or pyephem flips out and uses crazy values
        # for lat/lng, resulting in an AlwaysUp error.  Unfortunately, keeping
        # str() here results in a DeprecationWarning that's documented in a
        # (closed and ignored) bug here:
        #   https://github.com/brandon-rhodes/pyephem/issues/18
        self.observer.lat = str(lat)
        self.observer.lon = str(lng)

    def get_results(self):

        r = {"elements": [], "facets": [], "nationalities": []}

        if self._sun_up():
            r["elements"].append((Element.ELEMENT_DAY, self.WEIGHT_DAY))
        else:
            r["elements"].append((Element.ELEMENT_NIGHT, self.WEIGHT_NIGHT))
            if self._moon_full():
                r["elements"].append(
                    (Element.ELEMENT_MOON_FULL, self.WEIGHT_MOON_FULL)
                )
            elif self._moon_new():
                r["elements"].append(
                    (Element.ELEMENT_MOON_NEW, self.WEIGHT_MOON_NEW)
                )

        if self._mercury_up():
            r["facets"].append((Facet.FACET_MERCURY, self.WEIGHT_MERCURY))
        if self._venus_up():
            r["facets"].append((Facet.FACET_VENUS, self.WEIGHT_VENUS))
        if self._mars_up():
            r["facets"].append((Facet.FACET_MARS, self.WEIGHT_MARS))
        if self._jupiter_up():
            r["facets"].append((Facet.FACET_JUPITER, self.WEIGHT_JUPITER))
        if self._saturn_up():
            r["facets"].append((Facet.FACET_SATURN, self.WEIGHT_SATURN))
        if self._uranus_up():
            r["facets"].append((Facet.FACET_URANUS, self.WEIGHT_URANUS))
        if self._neptune_up():
            r["facets"].append((Facet.FACET_NEPTUNE, self.WEIGHT_NEPTUNE))

        return r

    def _sun_up(self):
        return self._check_transit(ephem.Sun)

    def _mercury_up(self):
        return self._check_transit(ephem.Mercury)

    def _venus_up(self):
        return self._check_transit(ephem.Venus)

    def _mars_up(self):
        return self._check_transit(ephem.Mars)

    def _jupiter_up(self):
        return self._check_transit(ephem.Jupiter)

    def _saturn_up(self):
        return self._check_transit(ephem.Saturn)

    def _uranus_up(self):
        return self._check_transit(ephem.Uranus)

    def _neptune_up(self):
        return self._check_transit(ephem.Neptune)

    def _moon_new(self):
        if self._check_transit(ephem.Moon):
            b = ephem.Moon()
            b.compute()
            if b.moon_phase < 0.05:
                return True
        return False

    def _moon_full(self):
        if self._check_transit(ephem.Moon):
            b = ephem.Moon()
            b.compute()
            if b.moon_phase > 0.95:
                return True
        return False

    def _check_transit(self, body):
        """
        Is `body` above the horizon?
        """

        body = body()

        next_rise = self.observer.next_rising(body).datetime()
        next_set = self.observer.next_setting(body).datetime()

        return (next_set - next_rise).total_seconds() < 0
