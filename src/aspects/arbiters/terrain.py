from PIL import Image
from io import StringIO
from urllib.request import urlopen

from django.core.cache import cache

from spirithunter.logging import LogMixin

from .base import Arbiter
from ..models import Element


class TerrainArbiter(LogMixin, Arbiter):

    WATER_THRESHOLD = 400
    NATURE_THRESHOLD = 400

    WATER_COLOURS = (
        (146, 179, 225),
        (147, 179, 222),
        (148, 179, 218),
        (149, 179, 215),
        (150, 179, 211),
        (151, 179, 208),
        (152, 179, 204),
        (155, 179, 215),
        (174, 205, 254),
        (177, 208, 254),
        (178, 208, 254),
    )
    NATURE_COLOURS = (
        (197, 216, 182),
        (202, 223, 170),
    )

    WEIGHT_WATER = 7
    WEIGHT_NATURE = 7

    CACHE_TIME = None  # Forever

    URL = ("http://maps.googleapis.com/maps/api/staticmap?center={},{}&zoom=17"
           "&size=512x512&maptype=roadmap&sensor=true")

    def __init__(self, lat, lng):
        """
        Fetch static image from Google API and dump it into self.colours
        """

        cache_key = "google-tile-%0.4f,%0.4f" % (lat, lng)

        self.colours = cache.get(cache_key)
        if not self.colours:
            url = self.URL.format(lat, lng)
            self.logger.info("Querying Google: {url}".format(url=url))
            tile = Image.open(StringIO(urlopen(url).read())).quantize(
                colors=16, palette=Image.WEB).convert("RGB")
            self.colours = {v: k for k, v in tile.getcolors()}
            cache.set(cache_key, self.colours, self.CACHE_TIME)

    def get_results(self):

        r = {"elements": [], "facets": [], "nationalities": []}

        if self._water():
            r["elements"].append((Element.ELEMENT_WATER, self.WEIGHT_WATER))
        if self._nature():
            r["elements"].append((Element.ELEMENT_NATURE, self.WEIGHT_NATURE))
        return r

    def _water(self):
        return self._check_terrain("water", self.WATER_THRESHOLD)

    def _nature(self):
        return self._check_terrain("nature", self.NATURE_THRESHOLD)

    def _check_terrain(self, group, threshold):
        """
        Parse self.colours for the index colour
        """

        colours = self.WATER_COLOURS
        if group == "nature":
            colours = self.NATURE_COLOURS

        total = 0
        for colour in colours:
            try:
                total += self.colours[colour]
            except KeyError:
                pass

        if total > threshold:
            return True

        return False
