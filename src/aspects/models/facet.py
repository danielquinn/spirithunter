from django.db import models
from django.templatetags.static import static


class Facet(models.Model):
    """
    An aspect assigned to a spirit that has no real effect.  This is simply
    here to differentiate the spirits from each other.
    """

    FACET_MERCURY = 1
    FACET_VENUS = 2
    FACET_MARS = 3
    FACET_JUPITER = 4
    FACET_SATURN = 5
    FACET_URANUS = 6
    FACET_NEPTUNE = 7
    FACET_PI = 8
    FACET_STARWARS = 9
    FACET_TOWEL = 10

    FACETS = (
        (FACET_MERCURY, "Mercury"),
        (FACET_VENUS, "Venus"),
        (FACET_MARS, "Mars"),
        (FACET_JUPITER, "Jupiter"),
        (FACET_SATURN, "Saturn"),
        (FACET_URANUS, "Uranus"),
        (FACET_NEPTUNE, "Neptune"),
        (FACET_PI, "Pi Day"),
        (FACET_STARWARS, "Star Wars Day"),
        (FACET_TOWEL, "Towel Day"),
    )

    name = models.CharField(max_length=128)
    slug = models.SlugField()

    class Meta:
        app_label = "aspects"

    def __str__(self):
        return dict(self.FACETS)[self.pk]

    @property
    def image16(self):
        return self._get_image(size=16)

    @property
    def image32(self):
        return self._get_image(size=32)

    def _get_image(self, size=32):
        return static("aspects/img/facets/{size}/{slug}.png".format(
            size=size,
            slug=self.slug
        ))
