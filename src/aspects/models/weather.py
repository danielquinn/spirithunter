from datetime import datetime
from dateutil.relativedelta import relativedelta

from django.contrib.gis.db import models
from django.db.models import F

from ..exceptions import ApiLimitExceeded


class WeatherCounter(models.Model):
    """
    Wunderground has a limit on the number of calls they allow per day, and per
    minute, so we keep track of the calls here and throw an exception if the
    limit is exceeded.

    Note that the times in this table are *not* UTC because Wunderground is
    US-centric and as such, a pain in the ass.
    """

    LIMITER_DAY = 1
    LIMITER_MINUTE = 2
    LIMITERS = (
        (LIMITER_DAY, "Day"),
        (LIMITER_MINUTE, "Minute"),
    )

    class Meta:
        app_label = "aspects"
        ordering = ("time",)

    CALL_LIMIT_DAY = 495  # 5-call wiggle-room for race conditions
    CALL_LIMIT_MINUTE = 9    # 1-call wiggle-room for race conditions

    time = models.DateTimeField()
    call_total = models.PositiveIntegerField(default=0)
    limit_type = models.PositiveIntegerField(choices=LIMITERS)

    def __str__(self):
        return "{}: {} @ {}".format(
            self.get_limit_type_display(),
            self.call_total,
            self.time,
        )

    @classmethod
    def count(cls):

        # Account for US Eastern Standard time being the cut-off
        now = datetime.utcnow() - relativedelta(hours=5)

        now_day = datetime(now.year, now.month, now.day)
        now_minute = datetime(
            now.year, now.month, now.day, now.hour, now.minute)

        counter_day, __ = cls.objects.get_or_create(
            limit_type=cls.LIMITER_DAY, time=now_day)
        counter_minute, __ = cls.objects.get_or_create(
            limit_type=cls.LIMITER_MINUTE, time=now_minute)

        if counter_day.call_total >= cls.CALL_LIMIT_DAY:
            raise ApiLimitExceeded()
        if counter_minute.call_total >= cls.CALL_LIMIT_MINUTE:
            raise ApiLimitExceeded()

        cls.objects.filter(
            limit_type=cls.LIMITER_DAY,
            time=counter_day.time
        ).update(call_total=F("call_total")+1)

        cls.objects.filter(
            limit_type=cls.LIMITER_MINUTE,
            time=counter_minute.time
        ).update(call_total=F("call_total")+1)
