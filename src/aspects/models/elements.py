from django.db import models
from django.templatetags.static import static
from django_extensions.db.fields import AutoSlugField


class Element(models.Model):
    """
    Elements have an effect on the spirit's ability to deal and take damage.
    """

    # These values are hard-coded as fixtures

    ELEMENT_DAY = 1
    ELEMENT_NIGHT = 2
    ELEMENT_MOON_NEW = 3
    ELEMENT_MOON_FULL = 4
    ELEMENT_WATER = 5
    ELEMENT_NATURE = 6
    ELEMENT_HOT = 7
    ELEMENT_COLD = 8
    ELEMENT_WIND = 9

    ELEMENTS = (
        (ELEMENT_DAY,       "Day"),
        (ELEMENT_NIGHT,     "Night"),
        (ELEMENT_MOON_NEW,  "New Moon"),
        (ELEMENT_MOON_FULL, "Full Moon"),
        (ELEMENT_WATER,     "Water"),
        (ELEMENT_NATURE,    "Nature"),
        (ELEMENT_HOT,       "Hot"),
        (ELEMENT_COLD,      "Cold"),
        (ELEMENT_WIND,      "Wind"),
    )

    name = models.CharField(max_length=128)
    slug = AutoSlugField(populate_from="name")

    class Meta:
        app_label = "aspects"

    def __str__(self):
        return dict(self.ELEMENTS)[self.pk]

    @property
    def image16(self):
        return self._get_image(size=16)

    @property
    def image32(self):
        return self._get_image(size=32)

    def _get_image(self, size=32):
        return static("aspects/img/elements/{size}/{slug}.png".format(
            size=size,
            slug=self.slug
        ))
