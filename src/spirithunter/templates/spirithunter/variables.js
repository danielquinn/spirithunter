/*
 *
 * Server-generated Javascript to make client-side programming DRY-er.
 *
 */
window.spirithunter = {
  location: [51.505, -0.09],  // London is the default because whatever.
  debug: {{ debug|yesno:'true,false,false' }},
  urls: {
    finder: "{% url 'spirits-finder' %}",
    spirits: {
      detail: "{% url 'spirits-detail' pk=0 %}"
    },
    api: {
      items: {
        listing: "{% url 'drf:item-list' %}",
        detail: "{% url 'drf:item-detail' pk='0000' %}"
      },
      inventory: {
        listing: "{% url 'drf:inventory-list' %}",
        detail: "{% url 'drf:inventory-detail' pk='0000' %}"
      },
      spirits: {
        listing: "{% url 'drf:spirit-list' %}",
        detail: "{% url 'drf:spirit-detail' pk='0000' %}"
      },
      users: {
        listing: "{% url 'drf:user-list' %}",
        detail: "{% url 'drf:user-detail' pk='0000' %}"
      }
    }
  }
};
