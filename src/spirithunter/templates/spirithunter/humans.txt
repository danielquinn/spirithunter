# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

  Daniel Quinn
  IA, server-side, client-side, and co-founder
  @searchingfortao
  http://danielquinn.org/

  Stephanie Hobson
  Original concept and co-founder
  @stephaniehobson
  http://www.stephaniehobson.com/

# THANKS

  The OpenData movement, without which ideas and projects like this one could
  never have been possible.

# TECHNOLOGY COLOPHON

  Front-end:
    HTML5, CSS3, Javascript
    jQuery, Modernizr, Twitter Bootstrap

  Back-end:
    Python
      Pillow
      Arrow
      Celery
      Django
      Django Countries
      Django Polymorphic
      Django REST Framework
      docutils
      psycopg2
      Pyephem
      PyTZ
      Requests
      ujson
    PostgreSQL, PostGIS, RabbitMQ, and Docker
