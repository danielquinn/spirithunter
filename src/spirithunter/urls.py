from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.staticfiles import views

from rest_framework import routers

from items.views import (
    ItemViewSet,
    InventoryViewSet,
    RecipeViewSet,
    IngredientViewSet
)
from spirits.views import SpiritViewSet, SpiritFinderViewSet
from users.views import UserViewSet

from .views import IndexView as Index
from .views import JavascriptVariablesView as JavascriptVariables
from .views import HumansView as Humans
from .views import RobotsView as Robots
from .views import AboutView as About
from .views import DefaultView as Default

admin.autodiscover()

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r"users", UserViewSet)
router.register(r"explore", SpiritFinderViewSet)
router.register(r"spirits", SpiritViewSet)
router.register(r"items", ItemViewSet)
router.register(r"inventory", InventoryViewSet)
router.register(r"recipes", RecipeViewSet)
router.register(r"ingredients", IngredientViewSet)

urlpatterns = [

    url(r'^api/', include(router.urls, namespace="drf")),

    url(r'^variables.js', JavascriptVariables.as_view(), name="javascript"),

    url(r'^$', Index.as_view(), name="index"),
    url(r"^about/$", About.as_view(), name="about"),
    url(r"^default/$", Default.as_view(), name="default"),

    url(r"^users/", include("users.urls")),
    url(r"^spirits/", include("spirits.urls")),
    url(r"^items/", include("items.urls")),

    url(r'^humans.txt$', Humans.as_view(), name="humanstxt"),
    url(r'^robots.txt$', Robots.as_view(), name="robotstxt"),

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(
        r'^api-auth/',
        include('rest_framework.urls', namespace='rest_framework')
    ),

]

if settings.DEBUG:
    urlpatterns += [
        url(r'^static/(?P<path>.*)$', views.serve, {"show_indexes": True}),
        url(r'', include('django.contrib.staticfiles.urls')),
    ]
