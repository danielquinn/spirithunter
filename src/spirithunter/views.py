from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.views.generic import TemplateView, View

from spirits.models import Spirit


class IndexView(View):

    template_name = "spirithunter/index.html"

    def get(self, request):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse("default"))
        return TemplateResponse(
            request=self.request,
            template=self.template_name,
            context={},
        )


class JavascriptVariablesView(TemplateView):

    template_name = "spirithunter/variables.js"

    def get_context_data(self, **kwargs):
        context = TemplateView.get_context_data(self, **kwargs)
        context.update({
            "debug": settings.DEBUG,
            "constants": {
                "Spirit": {
                    "ACTIVITY_FROLIC": Spirit.ACTIVITY_FROLIC,
                    "ACTIVITY_JARRED": Spirit.ACTIVITY_JARRED,
                    "ACTIVITY_WANDER": Spirit.ACTIVITY_WANDER,
                    "ACTIVITY_KILLED": Spirit.ACTIVITY_KILLED,
                }
            }
        })
        return context


class DefaultView(LoginRequiredMixin, TemplateView):
    template_name = "spirithunter/default.html"

    def get_context_data(self, **kwargs):
        return TemplateView.get_context_data(self, **kwargs)


class AboutView(TemplateView):
    template_name = "spirithunter/about.html"

    def get_context_data(self, **kwargs):
        context = TemplateView.get_context_data(self, **kwargs)
        context.update({
            "header": "About Spirithunter",
            "subheader": "Everything you need to know",
        })
        return context


class RobotsView(TemplateView):
    template_name = "spirithunter/robots.txt"


class HumansView(TemplateView):
    template_name = "spirithunter/humans.txt"
