from channels.routing import route_class
from spirits.consumers import FightConsumer

routing = [
    route_class(FightConsumer),
]
