def nav_name(request):
    """
    Handles which part of the navigation to highlight based on the current
    path.
    """

    path = request.get_full_path()

    return {
        "nav": {
            "about":        "active" if path == "/about/" else "",
            "registration": "active" if path == "/users/registration/" else "",
        }
    }
