/*
 *
 * Attach the current location to spirithunter.location.  It may also be a good
 * idea here to re-acquire the lat/lng every n minutes.
 *
 */
navigator.geolocation.getCurrentPosition(
  function(position){  // Position found
    window.spirithunter.location = [
      position.coords.latitude,
      position.coords.longitude
    ];
    $(document).trigger("location.acquired");
  },
  function(){
    console.log("Position not found, sticking with the default");  // Position not found
    $(document).trigger("location.acquired");
  },
  {
    enableHighAccuracy: false,
    timeout: (window.location.href.contains("localhost") ? 1 : 5000),  // Hack for browser running on a desktop talking to a dev server
    maximumAge: 30000
  }
);


/**
 *
 * Attach the current location to every ajax request.
 *
 */
$(document).ajaxSend(function(event, request, settings) {
  request.setRequestHeader("X-Location", window.spirithunter.location.toString());
});
