/*
 *
 * Extend Javascript primitives because Javascript is frustratingly limited.
 *
 */

String.prototype.capitalise = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};
