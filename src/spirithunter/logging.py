import logging


class LogMixin(object):
    """
    Use this mixin to do logging:

      self.logger.debug("My debugging message")
    """
    _logger = None

    @property
    def logger(self):

        if self._logger:
            return self._logger

        self._logger = logging.getLogger(
            '.'.join([__name__, self.__class__.__name__]))

        return self.logger

