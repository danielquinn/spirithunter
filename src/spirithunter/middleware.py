import json

from django.contrib.gis.geos import fromstr
from django.http import HttpResponseBadRequest

from spirithunter.logging import LogMixin


class LocationMiddleware(LogMixin):
    """
    Process all requests looking for evidence of a location.  If it's in the
    body of the request, use that.  Otherwise, use whatever is stated in the
    X-Location header.  If nothing is specified, .location will remain None.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        self.logger.debug("Location middleware start")
        if "/api/" not in request.get_full_path():
            self.logger.debug("No location attached due to URL")
            # It's cool, non-API calls don't require a location anyway
            return self.get_response(request)

        location = request.META.get("HTTP_X_LOCATION")
        if request.method == "GET":
            location = request.GET.get("location", location)
        elif request.method == "POST":
            location = request.POST.get("location", location)
        else:
            try:
                json.loads(request.body).get("location")
            except ValueError:
                pass  # We go with whatever was in the header or nothing

        self.logger.debug("Location is %s", location)
        if not location:
            return HttpResponseBadRequest("A location must be provided")

        request.location = self._parse_location(location)

        return self.get_response(request)

    def _parse_location(self, location):

        self.logger.debug("Parsing location")

        if not location:
            return None

        try:
            lat, lng = location.split(",")
            lat, lng = float(lat), float(lng)
        except ValueError:
            self.logger.debug("Parsing failure")
            return None

        self.logger.debug("Location data found: %s, %s", lat, lng)

        return fromstr("POINT(%s %s)" % (lng, lat))
