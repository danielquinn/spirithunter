# Spirithunter (working title)

Long before Pokemon Go was a glimmer in their marketing department's eye, this
project was being developed by me and [Stephanie Hobson](http://stephaniehobson.ca/)
in response to the loss of Gowalla which consumed a great deal of our time.

Unfortunately though, life got in the way, so "Spirithunter" was abandoned for
a while.  The code is still pretty nifty though: generating monsters based on
geographical coordinates that feed into multiple APIs to generate attributes
unique to the area.  I'm really proud of this work, and wish I'd had more
time for it.  Maybe now that I've got some more time on my hands, I can make it
useful again.

> This project is now under active development again and presently most stuff
> doesn't work.  It will soon though.


## Setting up Your Dev Environment

1. Clone this repo into somewhere convenient.
2. You'll need to pull down the latest Docker image, so run
   `docker pull danielquinn/spirithunter` and wait while Docker does its thing.
3. Now that you have a working image, you just need the external service
   dependencies like a database and queue server.  Docker Compose will hook
   you up.  Just run `docker-compose up`.  As this is your first time, this
   will do a lot of things:
    * Download the standard containers for PostgreSQL/PostGIS, Redis, and
      RabbitMQ
    * Start up all of the components
    * Run the Django migrations to create the tables
    * Run the initial data fixtures to populate the database

After that, you're ready to go.  Leave this window open so you can refer to
it for debugging output and in another window, try running the tests just to
be sure stuff is working as it should:

    $ <git-project-root>/scripts/runtests

You should see some pretty output with little green checkmarks.


## Now What?

If everything worked out alright, you should be able to visit
http://127.0.0.1:8000 and see the website.  Additionally, you can check out the
[REST API](http://127.0.0.1:8000) through the nifty browseable interface.


## Where Is Everything?

It's a standard Django setup, and you'll find everything under `src/`.  I've
been fiddling with the locations of some of the static files, and I might still
move them around.

The apps are broken down like this:


### aspects

Things attached to spirits such as `Element`s and `Facet`s.  The first plays an
active role in the nature of the spirit (nature, water, etc.), the second is
just fun info like nationality and planet.


### contrib

Modules that weren't proper Python modules which I nonetheless wanted to
include.  Currently this is just home to the naming code I found.


### economy

What it sounds like.  This is where the money system is stored.


### geography

We needed a way to tell where a user was coming from so we could come up with
spirit nationality, so that stuff lives here.


### items

Stuff you can buy and use.  The `Item` model is polymorphic, supporting
subclasses like `SpiritItem` and `ReagentItem`.


### spirithunter

The project-wide, special directory.  This is where the Django settings are
stored, the site-wide `urls.py`, etc.


### spirits

The `Spirit` model lives here, as do the `Family` and `Level` models.


### users

We're making use of Django 1.5+'s ability to override the Django user model
here.  Additionally, the `/username/spirits/` and `/username/items/` pages live
here.
