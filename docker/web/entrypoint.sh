#!/usr/bin/env sh

echo ${SPIRITHUNTER_IS_PRODUCTION}

while ! echo "SELECT 1" | psql -U postgres -h postgres >& /dev/null; do
  echo "Waiting for PostgreSQL"
  sleep 1
done

if [[ "${SPIRITHUNTER_IS_PRODUCTION}" = "1" ]]; then
  echo "Set this up:"
  echo "  https://www.reddit.com/r/django/comments/51xu72/channels_adopted_as_an_official_django_project/d7fspl0"
elif [[ "${SPIRITHUNTER_AUTO_RUNSERVER}" = "1" ]]; then
  /app/src/manage.py runserver 0.0.0.0:8000 --verbosity 3
else
  /app/src/manage.py runserver 0.0.0.0:8000 --verbosity 3
fi

