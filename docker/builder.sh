#!/bin/sh

HERE=$(dirname ${0})

sudo rm -f $(find ${HERE}/../ -name '*.pyc')
sudo rm -rf $(find ${HERE}/../ -name '__pycache__')

docker build -t danielquinn/spirithunter -f ${HERE}/../Dockerfile ${HERE}/..
