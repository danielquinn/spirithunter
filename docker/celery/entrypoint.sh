#!/usr/bin/env sh

while ! echo "SELECT 1" | psql -U postgres -h postgres >& /dev/null; do
  echo "Waiting for PostgreSQL"
  sleep 1
done

# Migrate on every start regardless
/app/src/manage.py migrate

# If there are no items, then we haven't done the initial fixtures yet
number_of_items=$(
  echo "SELECT COUNT(*) FROM items_item" \
  | psql -U postgres -h postgres \
  | head -n 3 \
  | tail -n 1 \
  | sed -e 's/^ *//'
)
if [[ ${number_of_items} = "0" ]]; then
  echo "Preparing database"
  /app/src/manage.py loaddata /app/src/geography/fixtures/initial_data.json
  /app/src/manage.py loaddata /app/src/aspects/fixtures/initial_data.json
  /app/src/manage.py loaddata /app/src/items/fixtures/initial_data.json
  /app/src/manage.py loaddata /app/src/spirits/fixtures/families.json
  /app/src/manage.py loaddata /app/src/spirits/fixtures/ladders.json
  /app/src/manage.py import_citytemp_data
fi

celery -A spirithunter worker -l info --workdir=/app/src/
