Here's everything you need to know to get started.  Obviously, this will all
end up in a Vagrant box though.

# System Requirements

While most of the dependencies are handled by way of virtualenv (see below),
there are of course some basics that every server must have:

* Apache, nginx, or some other django-friendly webserver
* PostgreSQL with GIS extensions

In addition to these basics, the following packages must exist in the system
or pip won't be able to compile the dependencies for the virtualenv:

* sci-libs/proj
* sci-libs/geos
* dev-db/postgis
* sci-libs/gdal

In order to get the python packages actually installed, you'll also need these:

* dev-python/virtualenv
* dev-vcs/git

The PostGIS server needs to have basic template data in there as well.  Run
this to get it setup [as seen here](https://docs.djangoproject.com/en/1.3/ref/contrib/gis/install/#post-installation).

* If the postgis db already exists and you want to rebuild it, do this first:

    psql -d postgres -c "UPDATE pg_database SET datistemplate='false' WHERE datname='template_postgis';"

* Otherwise, continue with this:

    POSTGIS_SQL_PATH=`pg_config --sharedir`/contrib/postgis-2.1
    createdb -E UTF8 template_postgis
    createlang -d template_postgis plpgsql # Adding PLPGSQL language support.
    psql -d postgres -c "UPDATE pg_database SET datistemplate='true' WHERE datname='template_postgis';"
    psql -d postgres -c "ALTER USER vagrant WITH PASSWORD 'vagrant';"
    psql -d template_postgis -f $POSTGIS_SQL_PATH/postgis.sql
    psql -d template_postgis -f $POSTGIS_SQL_PATH/spatial_ref_sys.sql
    psql -d template_postgis -c "GRANT ALL ON geometry_columns TO PUBLIC;"
    psql -d template_postgis -c "GRANT ALL ON geography_columns TO PUBLIC;"
    psql -d template_postgis -c "GRANT ALL ON spatial_ref_sys TO PUBLIC;"

## Important note if you're using PostgreSQL 9.1+ and Django <1.3.4

If you run into an error message like this:

    DatabaseError: invalid byte sequence for encoding "UTF8": 0x00

It's probably because of a known issue.  Looks like there's a bug in the
implementation of PostGIS and Django that requires a patch.  However the devs
don't intend to port the patch from trunk to previous releases, so the option
is to (a) apply the patch manually, or just change a setting in your
PostgreSQL setup.  I suggest the latter, [but the details are here](https://code.djangoproject.com/ticket/16778)

The change for PostgreSQL is just to set:

    standard_conforming_strings = off

in:

    /etc/postgresql-9.1/postgresql.conf


# The Development Environment

  ...is built using `virtualenv`, a tool that lets us construct a
  project-specific environment with our own libraries without
  depending/changing that of the host system.  As each server is different,
  we have to generate our controlled virtual environment.  To activate this
  environment in your shell, simply run the following from the site root
  (`trunk/www/`):

    # Create the virtual environment
    $ virtualenv --no-site-packages --distribute --python=$(which /usr/bin/python2.7) virtualenv

    # Enter our new, empty environment
    $ source virtualenv/bin/activate

    # Install all of the modules we need
    $ pip install --requirement=./trunk/api/django/settings/requirements.pip

    # If we're not using the python-based development webserver, we need to setup the staticfiles
    $ ./django/spirithunter/manage.py collectstatic

  That will result in a lot of output, where some of the CPython modules are
  compiled and the pure python ones are downloaded.  The `activate` step will
  also modify your shell environment so you can just do this:

    $ python

  and be sure that you've got access to all the libraries you're supposed to.
  More importantly, you must run this in order to run the development webserver:

    $ source virtualenv/bin/activate
    $ cd trunk/www/django/spirithunter/
    $ ./manage.py runserver

  Lastly, if you're using PyCharm, don't forget to set the python interpreter
  for this project to the `python2.7` binary found in `virtualenv/bin`.


# Documentation

  Documentation is generated from the code itself, so if you want to read it,
  you can either pop open the code and take a look, or cd into the scripts
  directory and run it yourself:

    $ cd trunk/www/scripts/
    $ ./makedocumentation


# Fun & Awesome Scripts

* `scripts/rebuilddb`
  Assuming your shell user has the rights, this scripts blows away the current
  database and replaces it with an empty, starter version of the database.
  Handy to run when you've changed too many things to keep track.

