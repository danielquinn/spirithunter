Questions:
    When you win a monster, do you ge them at the level you beat them, or level 1?
    What's to stop a user from just walking away from a battle?

Calls

  /users/<user_id>
    GET  Detail(username)
  /users/<username>/pack
    GET  ListOfSpirits(username)  [{},]
  /spirits/<id>
    GET  Detail(id)
    PUT  Modify(action=[let_out,release])
  /world/<lat>,<lng>
    GET  FindSpiritsNear(lat,lng)
      Creates encounters if <5 exist in the vicinity
      Returns a list of spirits in the vicinity complete with images and stats {spirits: [{spirit: {}, location: []},]}
  /encounters/n
    POST encounter(action=[item,attack1,attack2,attack3,run],fightid,lat,lng)
      Initiates a fight
      Returns results of action
    PUT encounter(action=[item,attack1,attack2,attack3,run],fightid)
      Returns results of action
  /items/
    GET
  /items/<id>
    GET


user has spirits in her pocket

spirit:
  strong/weak:
    water/law/hot/cold/speed/night/day
  3 attacks:
    - only does elemental damage
    - mix of primary/secondary element
    - non-elemental
  attacks are influenced by environment
  healing:
    over time
    can be "let out to play"
      improves healing factor
    is influenced by environment
  level-based (xp)
    based on the fights they get into
    evolves every 5 levels
      gains an affinity
      new image
  loot:
    items

items:
  jars
    can have elements
  magic stick
    temporary affinity
  spirit

input:
  location

onwin:
  win monster
  if release:
    get stuffs:
      gold
      sticks
      stick components

onloss:
  monster must go home

fight:
  highest agility goes first
  short: 3 turns for lesser spirits, 5 for bosses
  4 buttons: attack1,2,3,run

encounters:
  open phone
  scan for opponents
  appear at 30m intervals (roughly)
  walk to location and engage


Summoning monsters with an item

Affinities determine weaknesses only, attacks are either chosen to be elemental
or are the product of all elements attached to monster

at level milestones, spirits are automatically evolved to their higher state
based on the successful engagements with other spirits. ie. a water monster that
fights mostly nature monsters for 5 levels will become water/nature at level 5.

milestones at lv 20,40,60,80

How to call with curl:

    curl -v --user me@danielquinn.org:asdf --dump-header - -s -H "Content-Type: application/json" -H "Accept: application/json" -X POST -d '{"some": "data"}' 'http://localhost:8000/v1/encounters/'

Attributes (current health, attack, defence, etc.  Level is always visible) of
spirits should be invisible (show family only) unless the user has the
Librascope enabled.



Elements are attached to attack and defense and are incremented along with other
stats at level up time.  At milestones a new element is added with a low stat,
incrementing as levels continue to rise.

At level up the user is allocated a fixed set of points to allocate to base
stats, and a separate set of points to allocate to the spirit's elemental
abilities.

All aspects are integer values, and *not* percentages.



How attributes work according to Stephanie:


A spirit has day aspect of +2 and a water aspect of +1 and an Attack of 5.

A non elemental attack does 3 day damage and 2 water damage.

A element attack of day does 5 day damage.


The spirit defending has a defence of 2 and a day aspect of -2 and a water
aspect of +1.

From the non elemental attack it feels the effects of the attack as if it was
3+2 day damage and 2-1 water damage for a total of 6 damage which is then
reduced by its defence to 4 damage.

From the elemental attack it feels the effects of the attack as if it was 5+2
day damage for a total of 7 which the defence then reduces to 5.


In a situation where the environment has a +1 day aspect the poor defending
spirit suffers an additional point of day damage on any day attack.

If the spirit defending has a defence of 2 and a day aspect of +2.

From the non elemental attack it feels the effect of the attack as if it was
3-2 day damage and 2 water damage for a total of 3 which its defence then lowers
to 1.

From the elemental attack it feels the effects of the attack as if it was 5-2
day damage for a total of 3 which its defence then lowers to 1.


And, if I recall correctly this gets really messy at higher levels or when not
working with round numbers.

BUT we've discussed this several times since the notes were made and I'm not
sure any of this is accurate any more :/




Sample Fight Sequence

|                                           |
| User: attack bot with <action> ---------> |
|                                           |
| <------------- Bot Damage: n HP remain: n |
|                                           |
| <-------------- Bot: attack with <action> |
|                                           |
| <------------ User Damage: n HP remain: n |
|                                           |
| <-------------- Bot: attack with <action> |
|                                           |
| <------------ User Damage: n HP remain: n |
|                                           |
| User: attack bot with <action> ---------> |
|                                           |
| <------------ Bot Damage: n Victory: true |
|                                           |
| <----------------------- Spoils: [] XP: n |
|                                           |


### Websockets:

* phone to server: location
    * server generates spirits if none available
* server to phone: spirits nearby
* phone to server: engage spirit uuid
* phone to server: attack
    * server checks if attack is available yet based on spirit speed
    * server calculates damage
    * if victory, return victory:true, then return spoils
* server to phone: attack when action available
* server to phone: damage done
    * if defeated, return defeated:true


#### Architecture

**Important: we're not trying too hard to stop cheaters**

* There's no "fight server".  Instead, we react to heartbeat messages
    * Fight responses are gauged based on number of beats
    * If no beat comes, no action is taken and no actions are accepted
    * A cleanup script can go through every n minutes and end fights that
      weren't ended by the user.

* On explore: ws_connect
* On move (a significant distance or first time): ws_message: location
* On engage: ws_message: engagement
    * Sends fight-start message to the fight queue
    * Stores fight progress in the session


### REST

* View collection
* View inventory
* Purchases & transaction history
