

User

    UID - unique id for user
    UserName - display name for user
    Password Hash - password
    Email
    Brithday
    SeenByOthers - Privacy (on or off)
    HomeLatLong - co-ordinates of home
    HomeFinish - countdown timer until home is complete (it takes 24 hours to plant the home tree, so they can be moved but not without cost)


Spirits

    SID - unique id for spirit
    Level
    Spirit Type ID
    Attack
    Defence
    Speed
    Owner - references user table or is not owned - now that I think about it we’re going to have to find a way to tack the released spirits
    Current HP
    Max HP
    Given Name


Spirit Type

    Spirit Type ID
    Name - what kind of spirit it is
    Image1 - the image used to represent it until the first milstone
    Image2 - the iamge used to represent it until the second milestone.
    Hm, maybe images and levels need to be in a new table
    Origin
    common/uncommon/epic


Spirit Type / Affinity Type - joining table

    Spirit Type ID
    Affinity ID
    Value - negative values are for aversions


Temporary Spirits

    ID
    Spirit Type ID
    lat, long
    time to expire


Spirit History

    ID
    Event Type ID
    lat, long
    datetime
    user ID
    sprit ID
    first variable
    second variable - I can’t remember what these are for


Events

    ID
    text - I think this is intended to be something like “captured” “defeated” “released”


Items - This needs to be re-thought, it doesn’t hold both jars and sticks

    ID
    name
    description
    affinity
    affinity value
    level
    image


User/Item Joining table

    user
    item


Craft

    ID
    name
    description
    result - Item ID


Craft Recipe - can make items by combining other items, this table contains the combinations of items necessary

    Craft ID
    Reagent
    Count - how many of them


Reagents - reagents or maybe elements are another thing they can win in a fight and can be combined to make stuff, guess we need a table to track these too


Attacks

    ID
    Name


Attacks/Affinity Joining Table

    ID
    Attack ID
    Affinity ID
    Value (positive or negative)



API interactions

    user passes lat/long at connect
    api returns environment affinities
    user requests map
    api returns map with local spirits and local players
        originally we thought it would be a map tile with moving spirits on it but maybe a flat map would be easier? I’d be nice to be able to click on the spirt’s pic
        local spirits have level, spirit type name, and thumbnail
        could also have location, direction and speed for placement on map (maybe later)
    user returns what they want to interact with
        spirit - fight
        player - trade, fight, or team up (player interactions can be left for later in development)
        assume user returns spirit for this interaction
    api returns full stats of opponent (affinties, aversions, current HP, attack, defense) and a fight ID
    user requests list of available spirits (these would be spirits in jars on their belt, gonna have to track these some how)
    user returns which spirit to use
    api returns full stats of spirit
    user reques a list of items the they can use in this fight
    api returns items
    user returns which item to use or skips
    user requests fight conditions
    api returns adjusted environmental affinties based on item use, whether or not the user can run from this fight, and who goes first
    user requests available attacks
    user returns which attack to use
    round one - back end figures out who attacks first, picks an attack for the free spirit (wonder how we figure this out) then executes the attacks in the correct order
    user requests results of round one
    api returns who goes first, damage done, new total HP
    rinse repeat


okay it gets a little fuzzy after this, I think we need some kind of XP table and how to track if affinities and aversions are rising needs to be worked out

    fight is over when one spirit reaches 0 or if user runs, maybe some spirits can run too?
    if the free spirit runs out of HP first the user gets some experience (and maybe some gold) and may choose to keep or release it
    If they keep it it they can choose to put it in their belt or their cupboard (the cupboard is home)
        if the belt is full they can choose to send another spirit home to make room, spend the new spirt home, or release the new spirit
    If they release the spirit it grants them a boon(s)
        sticks, jars, gold, elements (for crafting)
    if they lost their spirit is at 0 HP it flees home and can’t be put back in a jar until it has healed.



While out a user can also play with their spirits

    letting them frolic can give them time to heal if they are in an environment with the right affinities (there is probably a time duration on this - 10 min of frolicing = a certain amount of healing, yea gods this could get complex)
    they can also break sticks to change the environmental affinities for healing purposes



Calculating fights and effects of affinities and aversions


Notes on spirits:

    two types of free spirits: epic and common
    common can be added to a collection with no problem
    epics can be kept for a maximum of 2 hours and they make continuing demands (for reagents, gold, or items)
        if their demands are not met they will leave and may do damage to spirits or items when they go
        if a rancher releases a spirit the have trained it may become an epic spirit
    spirits which fight a lot of spirits with the same origin get strong against it
    leveling should take x number of battles per level
        X increases on a bgeritmiz curve
        assumes fights against same level opponent
        each level above or below produces a % experience difference
    spirits can be common or uncommon, uncommon are harder to find
    spirits have 4 stats: attack, defence, health & speed
    spirits have 3 spells:
        attacks do typed damage based on affinties, affinties are augmented by environmental variables
        an environmental affinities are added to attacks and defences (if the affintity is a negative number it will result in a lower attack or defeence - we call this an aversion but aversions are not tracked seperately)
    Milestone levels require special events examples:
        must win 2 fights in a row
        must fight spirit with a certian oragin tyep
        must lose to creature with a critan oragin type
    spirits sent home can only be picked up by going home



Home has been talked about as a cupboard or as a tree you can hang the jars on
(I picture a tree full of glowing jars - very pretty). I think we decided on
tree in the end? The metaphor means if you wan to move your home it takes time
to put down roots again.

A fight should last between 5 to 7 rounds.

There is an element of randomness in each fight.
